<?php
###############################################################################################################################################################
###############################################################################################################################################################
###                                                                                                                                                         ###
###                                                                DEMOKRATIAN                                                                              ###
###                                                         http://demokratian.org                                                                          ###
###                                                  2015 CARLOS SALGADO WERNER (http://carlos-salgado.es)                                                  ###
###                                         Este programa ha sido creado por Carlos Salgado Werner                                                          ###
###                                                                                                                                                         ###
### Este programa es software libre. Puede redistribuirlo y/o modificarlo bajo los t�rminos de la Licencia P�blica General de GNU seg�n es publicada por la ###
### Free Software Foundation, bien de la versi�n 3 de dicha Licencia o bien de cualquier versi�n posterior.                                                 ###
### Este programa se distribuye con la esperanza de que sea �til, pero SIN NINGUNA GARANT�A, incluso sin la garant�a MERCANTIL impl�cita o sin garantizar   ###
### la CONVENIENCIA PARA UN PROP�SITO PARTICULAR. V�ase la Licencia P�blica General de GNU para m�s detalles.                                               ###
### Deber�a haber recibido una copia de la Licencia P�blica General junto con este programa. Si no ha sido as�, puede encontrarla en                        ###
### http://www.gnu.org/licenses/gpl-3.0.html                                                                                                                ###
### Si quieres participar en la mejora de este software ,eres libre de hacerlo,                                                                             ###
### Tambi�n puedes contactar con migo en el correo info@demokratian.org para trabajar en el desarrollo de forma colaborativa                                ###
###                                                                                                                                                         ###
###                                          Por favor, no elimines este aviso de licencia                                                                  ###
###                                                                                                                                                         ###
###############################################################################################################################################################
###############################################################################################################################################################
require_once("../config/config.inc.php");
require_once("conexion.php");



$url = explode("?",$_SERVER['HTTP_REFERER']);
$pag_referida=$url[0];
$redir=$pag_referida;


if ($_SERVER['HTTP_REFERER'] == ""){
die ("Error cod.:1 - Incorrect access");
exit;
}



if (isset($_POST['user']) && isset($_POST['pass'])) {




$db_conexion= mysqli_connect("$host", "$hostu", "$hostp") or die(header ("Location:  $url_vot/index.php?error_login=0"));
mysqli_select_db($db_conexion,"$dbn");

    //evitamos el sql inyeccion
    $user1 = mysqli_real_escape_string($db_conexion, $_POST['user']);
    //$user1 = $_POST['user'];
    $pass1 =  mysqli_real_escape_string($db_conexion, $_POST['pass']);

$usuario_consulta = mysqli_query($db_conexion,"SELECT ID,usuario,pass,id_provincia,nombre_usuario,apellido_usuario ,tipo_votante,bloqueo,id_ccaa,nivel_usuario,nivel_acceso,imagen_pequena,fecha_control,id_municipio,razon_bloqueo  FROM $tbn9 WHERE usuario='".$user1."'") or die(header ("Location:  $redir?error_login=1"));

 	 		
 
 if (mysqli_num_rows($usuario_consulta) != 0) {   
    $login = stripslashes($user1); 
    $password = md5($pass1);

 
 	$usuario_datos = mysqli_fetch_array($usuario_consulta);
    mysqli_free_result($usuario_consulta);

    mysqli_close($db_conexion);
    
    
    if ($login != $usuario_datos['usuario']) {
       	Header ("Location: $url_vot/index.php?error_login=4");
		exit;}


    if ($password != $usuario_datos['pass']) {
        Header ("Location: $url_vot/index.php?error_login=3");
	    exit;}
		
		
    if ($usuario_datos['bloqueo']=="si") {
        Header ("Location: $url_vot/index.php?error_login=7&tx=".$usuario_datos['razon_bloqueo']."");
	    exit;}
    
	
    unset($login);
    unset ($password);
	
			if ($civi==true){
			/*$datetime1 = $usuario_datos['fecha_control'];
			$datetime2 = date("Y-m-d");
			$interval = date_diff($datetime1, $datetime2);
			$interval->format('%R%a d�as');
			*/		
			}
  
    session_name($usuarios_sesion);
	session_start();
 
    
  
    session_cache_limiter('nocache,private');
    
   
    
    $_SESSION['ID']=$usuario_datos['ID'];
    $_SESSION['tipo_votante']=$usuario_datos['tipo_votante'];  
    $_SESSION['usuario_login']=$usuario_datos['usuario'];  
    $_SESSION['usuario_password']=$usuario_datos['pass'];
	$_SESSION['nombre_usu']=$usuario_datos['nombre_usuario'];
    $_SESSION['localidad']=$usuario_datos['id_provincia'];   
    $_SESSION['id_ccaa_usu']=$usuario_datos['id_ccaa'];
	$_SESSION['id_municipio']=$usuario_datos['id_municipio'];
    $_SESSION['id_subgrupo_usu']=$usuario_datos['id_subgrupo'];
	$_SESSION['nivel_usu']=$usuario_datos['nivel_usuario'];  //tipo de usuario (administardor, votante, etc
	$_SESSION['usuario_nivel']=$usuario_datos['nivel_acceso']; //nivel del usuario dentro del tipo
	$_SESSION['imagen']=$usuario_datos['imagen_pequena']; /////imagen del usuario
	
	$_SESSION['CKFinder_UserRole']='admin';// prueba a ver si asi funciona 
	
	///////////////// miramos el codigo de la provincia para meterlo en la sesion
    $options = "select DISTINCT ID,provincia from $tbn8  where ID = ".$usuario_datos['id_provincia']."";
	$resulta = mysqli_query($con, $options) or die("error: ".mysqli_error());
	while( $listrows = mysqli_fetch_array($resulta) ){$provin= $listrows['ID'];	
    $_SESSION['provincia']=$listrows['provincia'];   }
	
    ///////////////////////miramos la comunidad autonoma para meterlo tambien en la sesion
    
    $options2 = "select DISTINCT ID,ccaa from $tbn3  where ID = ".$usuario_datos['id_ccaa']." ";
	$resulta2 = mysqli_query($con, $options2) or die("error: ".mysqli_error());
	while( $listrows2 = mysqli_fetch_array($resulta2) ){
    $_SESSION['ccaa']=$listrows2['ccaa'];   }
	
	///////////////////////miramos el municipio para meterlo tambien en la sesion
    
    $options2 = "select DISTINCT nombre from $tbn18  where id_municipio = ".$usuario_datos['id_municipio']." ";
	$resulta2 = mysqli_query($con, $options2) or die("error: ".mysqli_error());
	while( $listrows2 = mysqli_fetch_array($resulta2) ){
    $_SESSION['municipio']=$listrows2['nombre'];   }

	
    $fecha =date("Y-m-d H:i:s");
    $insql = "UPDATE $tbn9 SET fecha_ultima =\"$fecha\" WHERE id=".$usuario_datos['ID']." ";
    $inres = @mysqli_query($con, $insql);
  
  
    Header ("Location: $PHP_SELF?");
    exit;
    
   } else {	   
      Header ("Location: $url_vot/index.php?error_login=2");
      exit;}
   } else {

     session_name($usuarios_sesion);
     session_start();

	if (!isset($_SESSION['usuario_login']) && !isset($_SESSION['usuario_password'])){	
	session_destroy();
	//die ("Error cod.: 2 - �Acceso incorrecto! <br> <a href=login.php>VOLVER A INTENTARLO</a>");
	Header ("Location: $url_vot/index.php?error_login=6");
	exit;
}
}

//a�adido para verificar que existe el nivel de usuario
	session_name($usuarios_sesion);
	session_start();
	if (!isset($_SESSION['usuario_nivel'])){	
	session_destroy();
	die ("Error cod.: 3 - �Acceso incorrecto! ");
	Header ("Location: $url_vot/interventores/index.php?error_login=6");
	exit;}
	
?>