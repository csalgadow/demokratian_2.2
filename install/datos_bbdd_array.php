<?php

$ccaa = array(
  array('ID' => '01','ccaa' => 'Andalucía','especial' => '0'),
  array('ID' => '02','ccaa' => 'Aragón','especial' => '0'),
  array('ID' => '03','ccaa' => 'Asturias, Principado de','especial' => '0'),
  array('ID' => '04','ccaa' => 'Balears, Illes','especial' => '0'),
  array('ID' => '05','ccaa' => 'Canarias','especial' => '0'),
  array('ID' => '06','ccaa' => 'Cantabria','especial' => '0'),
  array('ID' => '07','ccaa' => 'Castilla y León','especial' => '0'),
  array('ID' => '08','ccaa' => 'Castilla - La Mancha','especial' => '0'),
  array('ID' => '09','ccaa' => 'Catalunya','especial' => '0'),
  array('ID' => '10','ccaa' => 'Comunitat Valenciana','especial' => '0'),
  array('ID' => '11','ccaa' => 'Extremadura','especial' => '0'),
  array('ID' => '12','ccaa' => 'Galicia','especial' => '0'),
  array('ID' => '13','ccaa' => 'Madrid, Comunidad de','especial' => '0'),
  array('ID' => '14','ccaa' => 'Murcia, Región de','especial' => '0'),
  array('ID' => '15','ccaa' => 'Navarra, Comunidad Foral de','especial' => '0'),
  array('ID' => '16','ccaa' => 'País Vasco','especial' => '0'),
  array('ID' => '17','ccaa' => 'Rioja, La','especial' => '0'),
  array('ID' => '18','ccaa' => 'Ceuta','especial' => '0'),
  array('ID' => '19','ccaa' => 'Melilla','especial' => '0'),
  array('ID' => '20','ccaa' => 'Otros','especial' => '0')
);



$nivel_usuario = array(
  array('id' => '1','nivel_usuario' => 'Usuario'),
  array('id' => '2','nivel_usuario' => 'Administrador'),
  array('id' => '3','nivel_usuario' => 'Administrador provincia'),
  array('id' => '4','nivel_usuario' => 'Administrador CCAA'),
  array('id' => '5','nivel_usuario' => 'Interventor'),
  array('id' => '6','nivel_usuario' => 'Administrador Grupo Trabajo')
);



$provincia = array(
  array('ID' => '001','provincia' => 'Alava','especial' => '0','id_ccaa' => '016','correo_notificaciones' => ''),
  array('ID' => '002','provincia' => 'albacete','especial' => '0','id_ccaa' => '008','correo_notificaciones' => ''),
  array('ID' => '003','provincia' => 'alicante','especial' => '0','id_ccaa' => '010','correo_notificaciones' => ''),
  array('ID' => '004','provincia' => 'almería','especial' => '0','id_ccaa' => '001','correo_notificaciones' => ''),
  array('ID' => '005','provincia' => 'ávila','especial' => '0','id_ccaa' => '007','correo_notificaciones' => ''),
  array('ID' => '006','provincia' => 'badajoz','especial' => '0','id_ccaa' => '011','correo_notificaciones' => ''),
  array('ID' => '007','provincia' => 'islas baleares','especial' => '0','id_ccaa' => '004','correo_notificaciones' => ''),
  array('ID' => '008','provincia' => 'barcelona','especial' => '0','id_ccaa' => '009','correo_notificaciones' => ''),
  array('ID' => '009','provincia' => 'burgos','especial' => '0','id_ccaa' => '007','correo_notificaciones' => ''),
  array('ID' => '010','provincia' => 'cáceres','especial' => '0','id_ccaa' => '011','correo_notificaciones' => ''),
  array('ID' => '011','provincia' => 'cádiz','especial' => '0','id_ccaa' => '001','correo_notificaciones' => ''),
  array('ID' => '012','provincia' => 'castellón','especial' => '0','id_ccaa' => '010','correo_notificaciones' => ''),
  array('ID' => '013','provincia' => 'ciudad real','especial' => '0','id_ccaa' => '008','correo_notificaciones' => ''),
  array('ID' => '014','provincia' => 'córdoba','especial' => '0','id_ccaa' => '001','correo_notificaciones' => ''),
  array('ID' => '015','provincia' => 'coruña','especial' => '0','id_ccaa' => '012','correo_notificaciones' => ''),
  array('ID' => '016','provincia' => 'cuenca','especial' => '0','id_ccaa' => '008','correo_notificaciones' => ''),
  array('ID' => '017','provincia' => 'girona','especial' => '0','id_ccaa' => '009','correo_notificaciones' => ''),
  array('ID' => '018','provincia' => 'granada','especial' => '0','id_ccaa' => '001','correo_notificaciones' => ''),
  array('ID' => '019','provincia' => 'guadalajara','especial' => '0','id_ccaa' => '008','correo_notificaciones' => ''),
  array('ID' => '020','provincia' => 'guipúzcoa','especial' => '0','id_ccaa' => '016','correo_notificaciones' => ''),
  array('ID' => '021','provincia' => 'huelva','especial' => '0','id_ccaa' => '001','correo_notificaciones' => ''),
  array('ID' => '022','provincia' => 'huesca','especial' => '0','id_ccaa' => '002','correo_notificaciones' => ''),
  array('ID' => '023','provincia' => 'jaén','especial' => '0','id_ccaa' => '001','correo_notificaciones' => ''),
  array('ID' => '024','provincia' => 'león','especial' => '0','id_ccaa' => '007','correo_notificaciones' => ''),
  array('ID' => '025','provincia' => 'lleida','especial' => '0','id_ccaa' => '009','correo_notificaciones' => ''),
  array('ID' => '026','provincia' => 'la rioja','especial' => '0','id_ccaa' => '017','correo_notificaciones' => ''),
  array('ID' => '027','provincia' => 'lugo','especial' => '0','id_ccaa' => '012','correo_notificaciones' => ''),
  array('ID' => '028','provincia' => 'madrid','especial' => '0','id_ccaa' => '013','correo_notificaciones' => 'csalgadow@gmail.com'),
  array('ID' => '029','provincia' => 'málaga','especial' => '0','id_ccaa' => '001','correo_notificaciones' => ''),
  array('ID' => '030','provincia' => 'murcia','especial' => '0','id_ccaa' => '014','correo_notificaciones' => ''),
  array('ID' => '031','provincia' => 'navarra','especial' => '0','id_ccaa' => '015','correo_notificaciones' => ''),
  array('ID' => '032','provincia' => 'ourense','especial' => '0','id_ccaa' => '012','correo_notificaciones' => ''),
  array('ID' => '033','provincia' => 'asturias','especial' => '0','id_ccaa' => '003','correo_notificaciones' => ''),
  array('ID' => '034','provincia' => 'palencia','especial' => '0','id_ccaa' => '007','correo_notificaciones' => ''),
  array('ID' => '035','provincia' => 'las palmas','especial' => '0','id_ccaa' => '005','correo_notificaciones' => ''),
  array('ID' => '036','provincia' => 'pontevedra','especial' => '0','id_ccaa' => '012','correo_notificaciones' => ''),
  array('ID' => '037','provincia' => 'salamanca','especial' => '0','id_ccaa' => '007','correo_notificaciones' => ''),
  array('ID' => '038','provincia' => 'santa cruz de tenerife','especial' => '0','id_ccaa' => '005','correo_notificaciones' => ''),
  array('ID' => '039','provincia' => 'cantabria','especial' => '0','id_ccaa' => '006','correo_notificaciones' => ''),
  array('ID' => '040','provincia' => 'segovia','especial' => '0','id_ccaa' => '007','correo_notificaciones' => ''),
  array('ID' => '041','provincia' => 'sevilla','especial' => '0','id_ccaa' => '001','correo_notificaciones' => ''),
  array('ID' => '042','provincia' => 'soria','especial' => '0','id_ccaa' => '002','correo_notificaciones' => ''),
  array('ID' => '043','provincia' => 'tarragona','especial' => '0','id_ccaa' => '009','correo_notificaciones' => ''),
  array('ID' => '044','provincia' => 'teruel','especial' => '0','id_ccaa' => '002','correo_notificaciones' => ''),
  array('ID' => '045','provincia' => 'toledo','especial' => '0','id_ccaa' => '008','correo_notificaciones' => ''),
  array('ID' => '046','provincia' => 'valencia','especial' => '0','id_ccaa' => '010','correo_notificaciones' => ''),
  array('ID' => '047','provincia' => 'valladolid','especial' => '0','id_ccaa' => '007','correo_notificaciones' => ''),
  array('ID' => '048','provincia' => 'vizcaya','especial' => '0','id_ccaa' => '016','correo_notificaciones' => ''),
  array('ID' => '049','provincia' => 'zamora','especial' => '0','id_ccaa' => '007','correo_notificaciones' => ''),
  array('ID' => '050','provincia' => 'zaragoza','especial' => '0','id_ccaa' => '002','correo_notificaciones' => ''),
  array('ID' => '051','provincia' => 'ceuta','especial' => '0','id_ccaa' => '018','correo_notificaciones' => ''),
  array('ID' => '052','provincia' => 'melilla','especial' => '0','id_ccaa' => '019','correo_notificaciones' => ''),
  array('ID' => '053','provincia' => 'Domicilio fuera de España','especial' => '0','id_ccaa' => '020','correo_notificaciones' => '')
);


?>