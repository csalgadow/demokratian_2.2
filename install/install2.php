<?php
###############################################################################################################################################################
###############################################################################################################################################################
###                                                                                                                                                         ###
###                                                                DEMOKRATIAN                                                                              ###
###                                                         http://demokratian.org                                                                          ###
###                                                  2015 CARLOS SALGADO WERNER (http://carlos-salgado.es)                                                  ###
###                                         Este programa ha sido creado por Carlos Salgado Werner                                                          ###
###                                                                                                                                                         ###
### Este programa es software libre. Puede redistribuirlo y/o modificarlo bajo los términos de la Licencia Pública General de GNU según es publicada por la ###
### Free Software Foundation, bien de la versión 3 de dicha Licencia o bien de cualquier versión posterior.                                                 ###
### Este programa se distribuye con la esperanza de que sea útil, pero SIN NINGUNA GARANTÍA, incluso sin la garantía MERCANTIL implícita o sin garantizar   ###
### la CONVENIENCIA PARA UN PROPÓSITO PARTICULAR. Véase la Licencia Pública General de GNU para más detalles.                                               ###
### Debería haber recibido una copia de la Licencia Pública General junto con este programa. Si no ha sido así, puede encontrarla en                        ###
### http://www.gnu.org/licenses/gpl-3.0.html                                                                                                                ###
### Si quieres participar en la mejora de este software ,eres libre de hacerlo,                                                                             ###
### También puedes contactar con migo en el correo info@demokratian.org para trabajar en el desarrollo de forma colaborativa                                ###
###                                                                                                                                                         ###
###                                          Por favor, no elimines este aviso de licencia                                                                  ###
###                                                                                                                                                         ###
###############################################################################################################################################################
###############################################################################################################################################################
		header("Expires: Mon, 26 Jul 1997 05:00:00 GMT");             		
		header("Last-Modified: " . gmdate("D, d M Y H:i:s") . " GMT");		
		header("Cache-Control: no-cache, must-revalidate");           
		header("Pragma: no-cache");                                   

?>
<!DOCTYPE html>
<html lang="es"><head>
    <meta charset="utf-8">
    <title>Sistema de instalación de DEMOKRATIAN</title>
    
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <meta name="description" content=" ">
    <meta name="author" content=" ">
   <link rel="icon"  type="image/png"  href="../temas/demokratian/imagenes/icono.png"> 
        

    
    
    <!—[if lt IE 9]>
      <script src="https://html5shim.googlecode.com/svn/trunk/html5.js"></script>
    <![endif]—>
    <link href="../temas/demokratian/bootstrap/css/bootstrap.min.css" rel="stylesheet">
<!--    <link href="temas/emokratian/bootstrap/css/bootstrap-theme.min.css" rel="stylesheet">-->
    <link href="../temas/demokratian/estilo.css" rel="stylesheet">
    <!--<link href="temas/emokratian/estilo_login.css" rel="stylesheet">-->
  </head>
  
  <body>

 <div class="container">
 
    <!-- cabecera
    ================================================== -->
      <div class="page-header">
      <img src="cabecera_instalacion.png" class="img-responsive" alt="Logo <?php echo "$nombre_web"; ?>">
        
      </div>
      
    <!-- END cabecera
    ================================================== -->
   <div id="success"> </div> <!-- mensajes -->
  
   <div class="row" id="1_fase1">    
     <div class="col-lg-6">
            <div class="well">
              <h2>Instalador del sistema de votaciones DEMOKRATIAN</h2>

             
			 </div>
        </div>
  <div class="col-lg-6">
      <div class="well">
              <p> <div class="alert alert-danger">Recuerde borrar la carpeta <strong>"install" </strong>de su servidor una vez que haya terminado la instalación para evitar problemas.</div></p>             

			 </div>
        </div>
 </div>
  
  
  
   <div class="row" id="2_fase1">  
     <div class="col-lg-12">   
	<div class="well">
      <form action="prueba.php" method="post"  class="form-horizontal" role="form" name="FormBBDD" id="FormBBDD" >
        <h3 class="form-signin-heading">Segundo paso</h3>
         
         <div class="alert alert-info"> Tranquil@ , que estos datos podra modificarlos si es necesario posteriormente mediante el sistema de administracion de DEMOKRATIAN </div>
			
		<div id="success2"> </div>

        <fieldset>
         <legend>Otros datos de configuración</legend>
		<div class="form-group">
    	<label for="nombre_web" class="col-sm-4 control-label">Nombre de la web</label>
        <div class="col-sm-4">
        <input name="nombre_web" id="nombre_web" type="text" class="form-control" size="25"  placeholder="DEMOKRATIAN |  Centro de votaciones" required  autofocus /></td>
		</div>
        <div class="col-sm-4">
        
		</div>
        </div>               
 
        
        <div class="form-group">
        <label for="theme" class="col-sm-4 control-label">Tema del sitio</label> 
        <div class="col-sm-4">
                        <?php  
				$carpeta = "../temas"; //ruta actual
					if(is_dir($carpeta)){
						if($dir = opendir($carpeta)){
							while(($archivo = readdir($dir)) !== false){
								if($archivo != '.' && $archivo != '..' && $archivo != '.htaccess'&& $archivo != 'index.html'&& $archivo != 'index.php'){
								   
								   if($archivo=="demokratian"){
									   $check="selected=\"selected\" ";}
									   else { $check="";}
								   $lista .="<option value=\"".$archivo."\" $check > ".$archivo."</option>";
									$lista.=$archivo;
								}
							}
							closedir($dir);
						}
					}
                  ?>
                       <select name="theme" class="form-control"  id="theme" >
                        <?php echo "$lista";?>
                      </select>

        </div>
        <div class="col-sm-4">
		Si no ha añadido uno deje el tema por defecto "demokratian". 
        </div>
        </div>
        </fieldset>
        
        <fieldset>
         <legend>Configuración del servidor de correo</legend>
         
        <div class="form-group">
        <label for="theme" class="col-sm-4 control-label">servidor de correo</label> 
        <div class="col-sm-4">
        <input name="server_correo" type="text"  class="form-control" id="server_correo"  placeholder="smtp.MiServidor.net" size="25" required  autofocus /> 
        </div>
        <div class="col-sm-4">
		 
        </div>
        </div>
     
        <div class="form-group">
        <label for="user_correo" class="col-sm-4 control-label">Usuario  de correo</label> 
        <div class="col-sm-4">
        <input name="user_correo" type="text"  class="form-control" id="user_correo"  placeholder="nombre usuario del correo" size="25" required  autofocus /> 
        </div>
        <div class="col-sm-4">
		 
        </div>
        </div>
         
        <div class="form-group">
        <label for="pass_correo" class="col-sm-4 control-label">Contraseña</label>
        <div class="col-sm-4">
        <input name="pass_correo" id="pass_correo" type="text" class="form-control" size="25" placeholder="contraseña del correo" required autofocus />
        </div>
        <div class="col-sm-4"> Contraseña de correo.
        </div>
        </div>        
        
        <div class="form-group">
        <label for="dbhost" class="col-sm-4 control-label">Puerto del servidor de correo</label>
        <div class="col-sm-4">
        <input name="puerto_correo" id="puerto_correo" class="form-control" type="text" size="25" value="25" required  autofocus />
        </div>
        <div class="col-sm-4">
        Por defecto <strong>25</strong>.
        </div>
        </div>        
     
        <div class="form-group">
        <label for="dbhost" class="col-sm-4 control-label">Forma de envio de correo</label>
        <div class="col-sm-4">
       
         <label>
           <input name="tipo_envio" type="radio" id="tipo_envio_0" value="false" checked="CHECKED">
           IsSMTP</label>
          
         <label>
           <input type="radio" name="tipo_envio" value="true" id="tipo_envio_1">
           IsSendMail</label>
         <br>
       
		</div>
        <div class="col-sm-4">
        Algunos sevidores como 1&1 hay que usar IsSendMail. Por defecto <strong>IsSMTP</strong>.
        </div>
        </div>
        
        <div class="form-group">
        <label for="email_env" class="col-sm-4 control-label">Direccion de correo general</label>
        <div class="col-sm-4">
        <input name="email_env" id="email_env" class="form-control" type="text"   size="25" required  placeholder="info1@demokratian.org" autofocus />
        </div>
        <div class="col-sm-4">       
        * Puede usar las misma direccion de correo en los tres casos
        </div>
        </div>       
        
        
        <div class="form-group">
        <label for="email_error" class="col-sm-4 control-label">Correo de envios error</label>
        <div class="col-sm-4">
        <input name="email_error" id="email_error" class="form-control" type="text"  placeholder="info2@demokratian.org" size="25" required  autofocus />
        </div>
        <div class="col-sm-4">       
        Al que enviamos los correos de los que tienen problemas y no estan en la bbdd.
        </div>
        </div>       
         
  
        <div class="form-group">
        <label for="email_control" class="col-sm-4 control-label">Correo envios interventores</label>
        <div class="col-sm-4">
        <input name="email_control" id="email_control" class="form-control" type="text"  placeholder="info3@demokratian.org"  size="25" required  autofocus />
        </div>
        <div class="col-sm-4">       
        Direccion que envia el correo para el control con interventores.
        </div>
        </div>       
       
        <div class="form-group">
        <label for="email_error_tecnico" class="col-sm-4 control-label">Correo tecnicos</label>
        <div class="col-sm-4">
        <input name="email_error_tecnico" id="email_error_tecnico" class="form-control" type="text"  placeholder="info4@demokratian.org"  size="25" required  autofocus />
        </div>
        <div class="col-sm-4">       
        Correo electronico del responsable tecnico.
        </div>
        </div> 
        
        <div class="form-group">
        <label for="email_sistema" class="col-sm-4 control-label">Correo  del sistema</label>
        <div class="col-sm-4">
        <input name="email_sistema" id="email_sistema" class="form-control" type="text"  placeholder="info5@demokratian.org"  size="25" required  autofocus />
        </div>
        <div class="col-sm-4">       
        Demomento incluido en el envio de errores de la bbdd.
        </div>
        </div> 
        
        
        <div class="form-group">
        <label for="asunto_mens_error" class="col-sm-4 control-label">Asunto correo problemas acceso</label>
        <div class="col-sm-4">
        <input name="asunto_mens_error" id="asunto_mens_error" class="form-control" type="text"  placeholder="root de votaciones con problemas"  size="25" required  autofocus />
        </div>
        <div class="col-sm-4">       
        Asunto del mensaje de correo cuando hay problemas de acceso.
        </div>
        </div>    
        
        <div class="form-group">
        <label for="nombre_eq" class="col-sm-4 control-label">Asunto del correo</label>
        <div class="col-sm-4">
        <input name="nombre_eq" id="nombre_eq" class="form-control" type="text"  placeholder="Votaciones DEMOKRATIAN"  size="25" required  autofocus />
        </div>
        <div class="col-sm-4">       
        
        </div>
        </div>   
       
        <div class="form-group">
        <label for="nombre_sistema" class="col-sm-4 control-label">Nombre del sistema</label>
        <div class="col-sm-4">
        <input name="nombre_sistema" id="nombre_sistema" class="form-control" type="text"  placeholder="Sistema de  votaciones"  size="25" required  autofocus />
        </div>
        <div class="col-sm-4">       
        Nombre del sistema cuando se envia el correo de recupercion de clave.
        </div>
        </div>   
           
           
           <div class="form-group">
        <label for="asunto" class="col-sm-4 control-label">Asunto correo recuperar contraseña</label>
        <div class="col-sm-4">
        <input name="asunto" id="asunto" class="form-control" type="text"  placeholder="Recuperar tu contraseña en DEMOKRATIAN"  size="25" required  autofocus />
        </div>
        <div class="col-sm-4">       
       Texto del ssunto del correo para recuperar la contraseña.
        </div>
        </div>   
        
        </fieldset>	
        
        <div class="form-group">
      <div class="col-sm-offset-4 col-sm-6">
        <button class="btn btn-ttc btn-lg btn-primary btn-block" type="submit">Configurar Correos</button>
      </div>
      </div>
      </form>
     
    
     
        </div>
       
    </div> 
    
    </div>
    <div id="cargando" > <img class="cargador" src='../temas/demokratian/imagenes/loading.gif'/> <div  class="cargador2" > <h4>Esto puede tardar unos minutos...</h4></div>
    </div>
  <!--segunda fase de la configuración-->
   <div class="row" id="tercera_fase"> <div class="col-lg-12"> <br/>
  <p class="bg-success"> Ha terminado con la segunda fase de la configuración, enseguida terminamos, puede pasar a la tercera fase &nbsp;&nbsp;<a href="install3.php" class="btn btn-primary btn-lg active" role="button">Tercera fase</a></p>
   <br/></div>
   </div>
   <!--fin segunda fase-->

  <div id="footer" class="row">
  <?php  include("../temas/demokratian/pie.php"); ?>
   </div>
   
   
   
   
 </div>  
   
    <!--<script src="http://ajax.googleapis.com/ajax/libs/jquery/1.7.1/jquery.min.js"></script>-->
     <script src="../js/jquery-1.9.0.min.js"></script>
   
     <script type="text/javascript">
		$(document).ready(function() {
		$("#tercera_fase").hide();
		$("#cargando").hide();
		});
		
		$("#cargando").on("ajaxStart", function() {
		  // this hace referencia a la div con la imagen. 
		  $(this).show();
		}).on("ajaxStop", function() {
		  $(this).hide();
		});
	  </script>
     <!--<script src="js/jquery.validate.js"></script>-->
	<script src="../modulos/bootstrap-3.1.1/js/bootstrap.min.js"></script>
 	<script src="../js/jqBootstrapValidation.js"></script>
 	<script src="configura_2.js"></script>
   
  </body>
</html>