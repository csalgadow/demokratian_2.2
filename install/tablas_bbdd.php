<?php


$sql[] = "CREATE TABLE IF NOT EXISTS ".$prefijo."candidatos (
  ID int(6) unsigned zerofill NOT NULL AUTO_INCREMENT,
  id_provincia smallint(3) unsigned zerofill NOT NULL,
  texto text COLLATE utf8_spanish_ci NOT NULL,
  nombre_usuario tinytext COLLATE utf8_spanish_ci NOT NULL,
  sexo char(2) COLLATE utf8_spanish_ci NOT NULL DEFAULT 'H',
  id_votacion int(6) unsigned zerofill NOT NULL,
  anadido tinytext COLLATE utf8_spanish_ci NOT NULL,
  fecha_anadido datetime NOT NULL,
  modif tinytext COLLATE utf8_spanish_ci NOT NULL,
  fecha_modif datetime NOT NULL,
  id_vut int(6) NOT NULL DEFAULT '0',
  imagen tinytext COLLATE utf8_spanish_ci NOT NULL,
  imagen_pequena tinytext COLLATE utf8_spanish_ci NOT NULL,
  PRIMARY KEY (ID),
  UNIQUE KEY ID (ID)
) ENGINE=InnoDB  DEFAULT CHARSET=utf8 COLLATE=utf8_spanish_ci AUTO_INCREMENT=1 ;";



$sql[] = "CREATE TABLE IF NOT EXISTS ".$prefijo."ccaa (
  ID int(2) unsigned zerofill NOT NULL AUTO_INCREMENT,
  ccaa tinytext COLLATE utf8_spanish_ci NOT NULL,
  especial int(3) NOT NULL,
  PRIMARY KEY (ID)
) ENGINE=InnoDB   DEFAULT CHARSET=utf8 COLLATE=utf8_spanish_ci AUTO_INCREMENT=21 ;";


$sql[] = "CREATE TABLE IF NOT EXISTS ".$prefijo."debate_comentario (
  idcomentario int(11) NOT NULL AUTO_INCREMENT,
  id_usuario int(10) unsigned zerofill NOT NULL,
  id_votacion int(6) unsigned zerofill NOT NULL,
  tema tinytext CHARACTER SET utf8 COLLATE utf8_spanish_ci NOT NULL,
  fecha varchar(40) CHARACTER SET utf8 COLLATE utf8_spanish_ci NOT NULL,
  comentario text CHARACTER SET utf8 COLLATE utf8_spanish_ci NOT NULL,
  estado tinytext CHARACTER SET utf8 COLLATE utf8_spanish_ci NOT NULL,
  PRIMARY KEY (idcomentario),
  KEY id_usuario (id_usuario)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_spanish_ci AUTO_INCREMENT=1 ;";



$sql[] = "CREATE TABLE IF NOT EXISTS ".$prefijo."debate_preguntas (
  ID int(6) unsigned zerofill NOT NULL AUTO_INCREMENT,
  pregunta tinytext COLLATE utf8_spanish_ci NOT NULL,
  respuestas char(2) COLLATE utf8_spanish_ci NOT NULL,
  id_votacion int(6) unsigned zerofill NOT NULL,
  anadido tinytext COLLATE utf8_spanish_ci NOT NULL,
  fecha_anadido datetime NOT NULL,
  modif tinytext COLLATE utf8_spanish_ci NOT NULL,
  fecha_modif datetime NOT NULL,
  PRIMARY KEY (ID),
  UNIQUE KEY ID (ID)
) ENGINE=InnoDB  DEFAULT CHARSET=utf8 COLLATE=utf8_spanish_ci AUTO_INCREMENT=1 ;";


$sql[] = "CREATE TABLE IF NOT EXISTS ".$prefijo."debate_votos (
  ID int(10) NOT NULL AUTO_INCREMENT,
  id_votante int(10) unsigned zerofill NOT NULL,
  id_pregunta int(10) NOT NULL,
  voto char(2) COLLATE utf8_spanish_ci NOT NULL DEFAULT '0',
  id_votacion int(6) unsigned zerofill NOT NULL,
  codigo_val char(128) COLLATE utf8_spanish_ci NOT NULL,
  UNIQUE KEY ID (ID),
  KEY id_votacion (id_votacion)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_spanish_ci AUTO_INCREMENT=1 ;";


$sql[] = "CREATE TABLE IF NOT EXISTS ".$prefijo."elvoto (
  ID char(64) COLLATE utf8_spanish_ci NOT NULL,
  id_provincia int(3) unsigned zerofill NOT NULL,
  id_candidato smallint(6) unsigned zerofill NOT NULL,
  voto text COLLATE utf8_spanish_ci NOT NULL,
  id_votacion int(6) unsigned zerofill NOT NULL,
  codigo_val char(128) COLLATE utf8_spanish_ci NOT NULL,
  especial smallint(1) NOT NULL DEFAULT '0',
  incluido tinytext COLLATE utf8_spanish_ci NOT NULL,
  UNIQUE KEY ID (ID),
  KEY id_candidato (id_candidato),
  KEY id_votacion (id_votacion)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_spanish_ci;";


$sql[] = "CREATE TABLE IF NOT EXISTS ".$prefijo."grupo_trabajo (
  ID int(4) unsigned zerofill NOT NULL AUTO_INCREMENT,
  tipo smallint(1) NOT NULL DEFAULT '0',
  subgrupo tinytext COLLATE utf8_spanish_ci NOT NULL,
  especial int(3) NOT NULL,
  id_provincia int(3) NOT NULL,
  id_ccaa int(3) NOT NULL,
  texto text COLLATE utf8_spanish_ci NOT NULL,
  acceso smallint(1) NOT NULL,
  activo smallint(1) NOT NULL,
  creado smallint(10) unsigned zerofill NOT NULL,
  tipo_votante char(2) COLLATE utf8_spanish_ci NOT NULL,
  PRIMARY KEY (ID)
) ENGINE=InnoDB  DEFAULT CHARSET=utf8 COLLATE=utf8_spanish_ci AUTO_INCREMENT=1 ;";


$sql[] = "CREATE TABLE IF NOT EXISTS ".$prefijo."interventores (
  ID int(6) unsigned zerofill NOT NULL AUTO_INCREMENT,
  id_provincia smallint(3) unsigned zerofill NOT NULL,
  nombre tinytext COLLATE utf8_spanish_ci NOT NULL,
  apellidos tinytext COLLATE utf8_spanish_ci NOT NULL,
  correo tinytext COLLATE utf8_spanish_ci NOT NULL,
  id_votacion int(6) unsigned zerofill NOT NULL,
  anadido tinytext COLLATE utf8_spanish_ci NOT NULL,
  fecha_anadido datetime NOT NULL,
  modif tinytext COLLATE utf8_spanish_ci NOT NULL,
  fecha_modif datetime NOT NULL,
  pass tinytext COLLATE utf8_spanish_ci NOT NULL,
  usuario tinytext COLLATE utf8_spanish_ci NOT NULL,
  tipo int(1) NOT NULL DEFAULT '0',
  codigo_rec tinytext COLLATE utf8_spanish_ci NOT NULL,
  fecha_ultimo datetime NOT NULL,
  PRIMARY KEY (ID),
  UNIQUE KEY ID (ID)
) ENGINE=InnoDB  DEFAULT CHARSET=utf8 COLLATE=utf8_spanish_ci AUTO_INCREMENT=1 ;";


$sql[] = "CREATE TABLE IF NOT EXISTS ".$prefijo."municipios (
  id_municipio smallint(6) unsigned NOT NULL AUTO_INCREMENT,
  id_provincia smallint(6) NOT NULL,
  cod_municipio int(11) NOT NULL COMMENT 'Código de muncipio DENTRO de la provincia, campo no único',
  DC int(11) NOT NULL COMMENT 'Digito Control. El INE no revela cómo se calcula, secreto nuclear.',
  nombre varchar(100) NOT NULL DEFAULT '',
  PRIMARY KEY (id_municipio)
) ENGINE=InnoDB   DEFAULT CHARSET=utf8 AUTO_INCREMENT=8117 ;



";


$sql[] = "CREATE TABLE IF NOT EXISTS ".$prefijo."nivel_usuario (
  id int(2) NOT NULL AUTO_INCREMENT,
  nivel_usuario tinytext COLLATE utf8_spanish_ci NOT NULL,
  PRIMARY KEY (id)
) ENGINE=InnoDB   DEFAULT CHARSET=utf8 COLLATE=utf8_spanish_ci AUTO_INCREMENT=7 ;";




$sql[] = "CREATE TABLE IF NOT EXISTS ".$prefijo."paginas (
  id smallint(10) unsigned NOT NULL AUTO_INCREMENT,
  titulo tinytext NOT NULL,
  texto text NOT NULL,
  fecha date NOT NULL DEFAULT '0000-00-00',
  incluido tinytext NOT NULL,
  borrable char(2) NOT NULL DEFAULT 'si',
  pagina smallint(2) NOT NULL,
  zona_pagina smallint(2) NOT NULL,
  PRIMARY KEY (id)
) ENGINE=InnoDB  DEFAULT CHARSET=utf8 COLLATE=utf8_spanish_ci AUTO_INCREMENT=1 ;";


$sql[] = "CREATE TABLE IF NOT EXISTS ".$prefijo."provincia (
  ID int(3) unsigned zerofill NOT NULL AUTO_INCREMENT,
  provincia tinytext COLLATE utf8_spanish_ci NOT NULL,
  especial int(3) NOT NULL,
  id_ccaa int(3) unsigned zerofill NOT NULL,
  correo_notificaciones tinytext COLLATE utf8_spanish_ci NOT NULL,
  PRIMARY KEY (ID)
) ENGINE=InnoDB   DEFAULT CHARSET=utf8 COLLATE=utf8_spanish_ci AUTO_INCREMENT=54 ;";


$sql[] = " CREATE TABLE IF NOT EXISTS ".$prefijo."usuario_admin_x_provincia (
  ID int(10) unsigned zerofill NOT NULL AUTO_INCREMENT,
  id_usuario int(10) unsigned zerofill NOT NULL,
  id_provincia int(3) unsigned zerofill NOT NULL,
  admin int(1) unsigned zerofill NOT NULL DEFAULT '0',
  PRIMARY KEY (ID)
) ENGINE=InnoDB  DEFAULT CHARSET=utf8 COLLATE=utf8_spanish_ci AUTO_INCREMENT=1 ;";


$sql[] = "CREATE TABLE IF NOT EXISTS ".$prefijo."usuario_x_g_trabajo (
  ID int(10) unsigned zerofill NOT NULL AUTO_INCREMENT,
  id_usuario int(10) unsigned zerofill NOT NULL,
  id_grupo_trabajo int(4) unsigned zerofill NOT NULL,
  admin int(1) NOT NULL DEFAULT '0',
  estado int(1) NOT NULL DEFAULT '0',
  razon_bloqueo text COLLATE utf8_spanish_ci NOT NULL,
  PRIMARY KEY (ID),
  KEY id_grupo_trabajo (id_grupo_trabajo),
  KEY id_usuario (id_usuario)
) ENGINE=InnoDB  DEFAULT CHARSET=utf8 COLLATE=utf8_spanish_ci AUTO_INCREMENT=1 ;";


$sql[] = "CREATE TABLE IF NOT EXISTS ".$prefijo."usuario_x_votacion (
  ID int(10) unsigned NOT NULL AUTO_INCREMENT,
  id_provincia int(3) unsigned zerofill NOT NULL,
  id_votante int(10) unsigned zerofill NOT NULL,
  id_votacion int(6) unsigned zerofill NOT NULL,
  tipo_votante char(2) COLLATE utf8_spanish_ci NOT NULL,
  fecha datetime NOT NULL,
  correo_usuario tinytext COLLATE utf8_spanish_ci NOT NULL,
  ip tinytext COLLATE utf8_spanish_ci NOT NULL,
  forma_votacion tinytext COLLATE utf8_spanish_ci NOT NULL,
  PRIMARY KEY (ID),
  UNIQUE KEY ID (ID)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_spanish_ci AUTO_INCREMENT=1 ;";


$sql[] = "CREATE TABLE IF NOT EXISTS ".$prefijo."votacion (
  ID int(6) unsigned zerofill NOT NULL AUTO_INCREMENT,
  id_provincia int(3) unsigned zerofill NOT NULL,
  activa char(2) COLLATE utf8_spanish_ci NOT NULL DEFAULT 'no',
  nombre_votacion tinytext COLLATE utf8_spanish_ci NOT NULL,
  texto text COLLATE utf8_spanish_ci NOT NULL,
  resumen text COLLATE utf8_spanish_ci NOT NULL,
  tipo char(2) COLLATE utf8_spanish_ci NOT NULL DEFAULT '1',
  tipo_votante char(2) COLLATE utf8_spanish_ci NOT NULL DEFAULT '1',
  numero_opciones int(3) NOT NULL,
  anadido tinytext COLLATE utf8_spanish_ci NOT NULL,
  fecha_anadido datetime NOT NULL,
  modif tinytext COLLATE utf8_spanish_ci NOT NULL,
  fecha_modif datetime NOT NULL,
  fecha_com datetime NOT NULL,
  fecha_fin datetime NOT NULL,
  activos_resultados char(2) COLLATE utf8_spanish_ci NOT NULL DEFAULT 'no',
  fin_resultados char(2) COLLATE utf8_spanish_ci NOT NULL DEFAULT 'no',
  id_ccaa int(2) NOT NULL DEFAULT '0',
  id_subzona int(4) NOT NULL DEFAULT '0',
  id_grupo_trabajo int(4) unsigned zerofill NOT NULL DEFAULT '0000',
  demarcacion int(2) NOT NULL DEFAULT '0',
  seguridad int(2) NOT NULL,
  interventores int(1) NOT NULL DEFAULT '0',
  interventor char(2) COLLATE utf8_spanish_ci NOT NULL DEFAULT 'no',
  recuento int(1) NOT NULL DEFAULT '0',
  id_municipio smallint(6) NOT NULL DEFAULT '0',
  PRIMARY KEY (ID)
) ENGINE=InnoDB  DEFAULT CHARSET=utf8 COLLATE=utf8_spanish_ci COMMENT='tabla de las votaciones activas' AUTO_INCREMENT=1 ;
";


$sql[] = "CREATE TABLE IF NOT EXISTS ".$prefijo."votantes (
  ID int(10) unsigned zerofill NOT NULL AUTO_INCREMENT,
  id_provincia int(3) unsigned zerofill NOT NULL,
  id_municipio smallint(6) NOT NULL,
  nombre_usuario tinytext COLLATE utf8_spanish_ci NOT NULL,
  apellido_usuario tinytext COLLATE utf8_spanish_ci NOT NULL,
  nivel_usuario int(2) NOT NULL DEFAULT '1',
  nivel_acceso smallint(4) NOT NULL DEFAULT '10',
  correo_usuario tinytext COLLATE utf8_spanish_ci NOT NULL,
  nif tinytext COLLATE utf8_spanish_ci NOT NULL,
  id_ccaa int(3) NOT NULL DEFAULT '0',
  pass tinytext COLLATE utf8_spanish_ci NOT NULL,
  tipo_votante char(2) COLLATE utf8_spanish_ci NOT NULL,
  usuario tinytext COLLATE utf8_spanish_ci NOT NULL,
  fecha_ultima datetime NOT NULL,
  codigo_rec tinytext COLLATE utf8_spanish_ci NOT NULL,
  bloqueo char(2) COLLATE utf8_spanish_ci NOT NULL DEFAULT 'no',
  razon_bloqueo tinytext COLLATE utf8_spanish_ci NOT NULL,
  imagen varchar(200) COLLATE utf8_spanish_ci NOT NULL DEFAULT 'usuario.jpg',
  imagen_pequena varchar(200) COLLATE utf8_spanish_ci NOT NULL DEFAULT 'peq_usuario.jpg',
  perfil text COLLATE utf8_spanish_ci NOT NULL,
  fecha_control date NOT NULL,
  PRIMARY KEY (ID),
  KEY Usuario (usuario(512))
) ENGINE=InnoDB   DEFAULT CHARSET=utf8 COLLATE=utf8_spanish_ci COMMENT='Tabla donde se reflejan los votantes registrados' AUTO_INCREMENT=1 ;
";


$sql[] = "CREATE TABLE IF NOT EXISTS ".$prefijo."votantes_x_admin (
  id int(12) unsigned zerofill NOT NULL AUTO_INCREMENT,
  id_votante int(10) unsigned zerofill NOT NULL,
  id_admin int(10) unsigned zerofill NOT NULL,
  accion int(1) unsigned zerofill NOT NULL,
  fecha datetime NOT NULL,
  PRIMARY KEY (id)
) ENGINE=InnoDB  DEFAULT CHARSET=utf8 COLLATE=utf8_spanish_ci COMMENT='Tabla que registra quien ha incluido  o modificado votantes' AUTO_INCREMENT=1 ;

";


$sql[] = "CREATE TABLE IF NOT EXISTS ".$prefijo."votos (
  ID char(64) COLLATE utf8_spanish_ci NOT NULL,
  id_provincia int(3) unsigned zerofill NOT NULL,
  id_candidato smallint(6) unsigned zerofill NOT NULL,
  voto decimal(7,5) NOT NULL,
  id_votacion int(6) unsigned zerofill NOT NULL,
  codigo_val char(128) COLLATE utf8_spanish_ci NOT NULL,
  especial smallint(1) NOT NULL DEFAULT '0',
  incluido tinytext COLLATE utf8_spanish_ci NOT NULL,
  vote_id tinytext COLLATE utf8_spanish_ci NOT NULL,
  position int(3) NOT NULL,
  otros int(3) NOT NULL,
  UNIQUE KEY ID (ID),
  KEY id_candidato (id_candidato),
  KEY id_votacion (id_votacion)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_spanish_ci;
";
?>
