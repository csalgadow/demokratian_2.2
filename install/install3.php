<?php
###############################################################################################################################################################
###############################################################################################################################################################
###                                                                                                                                                         ###
###                                                                DEMOKRATIAN                                                                              ###
###                                                         http://demokratian.org                                                                          ###
###                                                  2015 CARLOS SALGADO WERNER (http://carlos-salgado.es)                                                  ###
###                                         Este programa ha sido creado por Carlos Salgado Werner                                                          ###
###                                                                                                                                                         ###
### Este programa es software libre. Puede redistribuirlo y/o modificarlo bajo los términos de la Licencia Pública General de GNU según es publicada por la ###
### Free Software Foundation, bien de la versión 3 de dicha Licencia o bien de cualquier versión posterior.                                                 ###
### Este programa se distribuye con la esperanza de que sea útil, pero SIN NINGUNA GARANTÍA, incluso sin la garantía MERCANTIL implícita o sin garantizar   ###
### la CONVENIENCIA PARA UN PROPÓSITO PARTICULAR. Véase la Licencia Pública General de GNU para más detalles.                                               ###
### Debería haber recibido una copia de la Licencia Pública General junto con este programa. Si no ha sido así, puede encontrarla en                        ###
### http://www.gnu.org/licenses/gpl-3.0.html                                                                                                                ###
### Si quieres participar en la mejora de este software ,eres libre de hacerlo,                                                                             ###
### También puedes contactar con migo en el correo info@demokratian.org para trabajar en el desarrollo de forma colaborativa                                ###
###                                                                                                                                                         ###
###                                          Por favor, no elimines este aviso de licencia                                                                  ###
###                                                                                                                                                         ###
###############################################################################################################################################################
###############################################################################################################################################################
		header("Expires: Mon, 26 Jul 1997 05:00:00 GMT");             		
		header("Last-Modified: " . gmdate("D, d M Y H:i:s") . " GMT");		
		header("Cache-Control: no-cache, must-revalidate");           
		header("Pragma: no-cache");                                   

?>
<!DOCTYPE html>
<html lang="es"><head>
    <meta charset="utf-8">
    <title>Sistema de instalación de DEMOKRATIAN</title>
    
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <meta name="description" content=" ">
    <meta name="author" content=" ">
   <link rel="icon"  type="image/png"  href="../temas/demokratian/imagenes/icono.png"> 
        

    
    
    <!—[if lt IE 9]>
      <script src="https://html5shim.googlecode.com/svn/trunk/html5.js"></script>
    <![endif]—>
    <link href="../temas/demokratian/bootstrap/css/bootstrap.min.css" rel="stylesheet">
<!--    <link href="temas/emokratian/bootstrap/css/bootstrap-theme.min.css" rel="stylesheet">-->
    <link href="../temas/demokratian/estilo.css" rel="stylesheet">
    <!--<link href="temas/emokratian/estilo_login.css" rel="stylesheet">-->
  </head>
  
  <body>

 <div class="container">
 
    <!-- cabecera
    ================================================== -->
      <div class="page-header">
      <img src="cabecera_instalacion.png" class="img-responsive" alt="Logo <?php echo "$nombre_web"; ?>">
        
      </div>
      
    <!-- END cabecera
    ================================================== -->
   <div id="success"> </div> <!-- mensajes -->
  
   <div class="row" id="1_fase1">    
     <div class="col-lg-6">
            <div class="well">
              <h2>Instalador del sistema de votaciones DEMOKRATIAN</h2>

             
			 </div>
        </div>
  <div class="col-lg-6">
      <div class="well">
              <p> <div class="alert alert-danger">Recuerde borrar la carpeta <strong>"install" </strong>de su servidor una vez que haya terminado la instalación para evitar problemas.</div></p>             

			 </div>
        </div>
 </div>
  
  
  
   <div class="row" id="2_fase1">  
     <div class="col-lg-12">   
	<div class="well">
      <form  method="post" name="FormBBDD"  class="form-horizontal" id="FormBBDD" role="form" >
        <h3 class="form-signin-heading">Tercer paso</h3>
        <h4 class="form-signin-heading">Ya solo queda saber los datos del administrador</h4>
         
			
		<div id="success2"> </div>
  

		<div class="form-group">
    	<label for="user" class="col-sm-4 control-label">Usuario  administrador</label>
        <div class="col-sm-4">
        <input name="user" id="user" type="text" class="form-control" size="25"  placeholder="demokratian" required  autofocus /></td>
		</div>
        <div class="col-sm-4">Entre 5 y 20 caracteres. No se pueden dejar espacios en blanco ni usar acentos o la ñ </div>
        </div>
        
        <div class="form-group">
        <label for="username" class="col-sm-4 control-label">Nombre y apellidos del administrador</label> 
        <div class="col-sm-4">
        <input name="username" id="username" type="text" class="form-control" size="25" placeholder="nombre_de_usuario" required  autofocus /> 
        </div>
        <div class="col-sm-4">
		 
        </div>
        </div>
        
        <div class="form-group">
        <label for="pass" class="col-sm-4 control-label">Contraseña</label>
        <div class="col-sm-4">
        <input name="pass" id="pass" type="text" class="form-control"  size="25" placeholder="contraseña"  autofocus />
        </div>
        <div class="col-sm-4">  
        </div>
        </div>        
        
        <div class="form-group">
        <label for="pass2" class="col-sm-4 control-label">Contraseña</label>
        <div class="col-sm-4">
        <input name="pass2" id="pass2" type="text" class="form-control"  size="25" placeholder="contraseña"  autofocus />
        </div>
        <div class="col-sm-4">  Vuelva a escribir su contraseña
        </div>
        </div>    
        
        <div class="form-group">
        <label for="email" class="col-sm-4 control-label">Correo electronico</label>
        <div class="col-sm-4">
        <input type="email" class="form-control" placeholder="Su correo electronico" id="email"  name="email" size="25" required  data-validation-required-message="Por favor, ponga su correo electronico" />
        
        </div>
        <div class="col-sm-4"></div>
        </div>  
             
       <div class="form-group">
        <label for="nif" class="col-sm-4 control-label">NIF</label>
        <div class="col-sm-4">
        <input name="nif" id="nif" type="text" class="form-control"  size="25" placeholder="Su nif"  autofocus />
        </div>
        <div class="col-sm-4"> 
        </div>
        </div>  

        <div class="form-group">
        <label for="dbhost" class="col-sm-4 control-label">Provincia en la participa el administrador</label>
        <div class="col-sm-4">
         <?php 
				  
require_once("../config/config.inc.php");

$tbn8=$extension."provincia";
 
$con= @mysqli_connect("$host","$hostu","$hostp") or die ("no se puede conectar"); 
mysqli_set_charset($con, "utf8");
$db = @mysqli_select_db($con, "$dbn") or die ("no se puede acceder a la tabla"); 
// listar para meter en una lista del tipo enlace
$activo=0;

		 
						$options = "select DISTINCT id, provincia, id_ccaa from $tbn8  order by ID";
						$resulta = mysqli_query($con, $options) or die("error: ".mysql_error($con));
						
						while( $listrows = mysqli_fetch_array($resulta) ){ 
						$id_pro = $listrows[id];
						$name1 = $listrows[provincia];
						$id_ccaa = $listrows[id_ccaa];
						
						$lista .="<option value=\"".$id_pro."#".$id_ccaa."\" > $name1</option>";
					}
					 ?>
                   
    	  <div class="form-group">   
           <select name="provincia" class="form-control"  id="provincia" >
                        <?php echo "$lista";?>
                      </select>
                      
                      
        
                 </div>
        </div>
        <div class="col-sm-4">
        
        </div>
        </div>        
        
         		<div id="municipio">  </div> 
        
        <div class="form-group">
      <div class="col-sm-offset-4 col-sm-6">
        <button class="btn btn-ttc btn-lg btn-primary btn-block" type="submit">Configurar Administrador</button>
      </div>
      </div>
      </form>
     
        </div>
       
    </div> 
    
    </div>
    <div id="cargando" > <img class="cargador" src='../temas/demokratian/imagenes/loading.gif'/> <div  class="cargador2" > <h4>Esto puede tardar unos minutos...</h4></div>
    </div>
  <!--segunda fase de la configuración-->
   <div class="row" id="final_fase"> <div class="col-lg-12"> <br/>
  <p class="bg-success"> Ha terminado con la instalación, todo parece haber ido bien, asi que deberia poder acceder a DEMOKRATIAN <a href="../index.php" class="btn btn-primary btn-lg active" role="button">Acceder</a></p>
   <br/>
    <div class="alert alert-danger">Recuerde borrar la carpeta <strong>"install" </strong>de su servidor para evitar problemas.</div>
  </div>
   </div>
   <!--fin segunda fase-->

  <div id="footer" class="row">
  <?php  include("../temas/demokratian/pie.php"); ?>
   </div>
   
   
   
   
 </div>  
   
    <!--<script src="http://ajax.googleapis.com/ajax/libs/jquery/1.7.1/jquery.min.js"></script>-->
     <script src="../js/jquery-1.9.0.min.js"></script>
     <script type="text/javascript">
		$(document).ready(function() {
		$("#final_fase").hide();
		$("#cargando").hide();
		});
		
		$("#cargando").on("ajaxStart", function() {
		  // this hace referencia a la div con la imagen. 
		  $(this).show();
		}).on("ajaxStop", function() {
		  $(this).hide();
		});
	  </script>
     <!--<script src="js/jquery.validate.js"></script>-->
	<script src="../modulos/bootstrap-3.1.1/js/bootstrap.min.js"></script>
 	<script src="../js/jqBootstrapValidation.js"></script>
 	<script src="configura_3.js"></script>
   	<script type='text/javascript' src='../js/admin_funciones.js'></script>
  
        <script type="text/javascript">
			$(document).ready(function(){
				$('#provincia').change(function(){
					
				var id_provincia=$('#provincia').val();
				$('#municipio').load('genera_select.php?id_provincia='+id_provincia);
				$("#municipio").html(data);
				
				});
			});		
	   </script>
  </body>
</html>