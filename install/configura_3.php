<?php 
require_once("../config/config.inc.php");
require_once("../inc_web/conexion.php");
include("../basicos_php/basico.php") ;

$user = fn_filtro($con,$_POST['user']);
$email = fn_filtro($con,$_POST['email']);
$pass = fn_filtro($con,$_POST['pass']);
$pass2 = fn_filtro($con,$_POST['pass2']);
$username = fn_filtro($con,$_POST['username']);
$nif = fn_filtro($con,$_POST['nif']);
$provinciaYccaa = fn_filtro($con,$_POST['provincia']);
$ides = explode("#", $provinciaYccaa);
$provincia= $ides[0];  
$id_ccaa= $ides[1];  
$id_municipio = fn_filtro($con,$_POST['id_municipio']);
	
	
function comprobar_nombre_usuario($nombre_usuario){ 
   //compruebo que el tamaño del string sea válido. 
   if (strlen($nombre_usuario)<4 || strlen($nombre_usuario)>21){ 
  
      $error="error1"; 
      return $error; 
   } 

   //compruebo que los caracteres sean los permitidos 
   $permitidos = "abcdefghijklmnopqrstuvwxyzABCDEFGHIJKLMNOPQRSTUVWXYZ0123456789-_"; 
   for ($i=0; $i<strlen($nombre_usuario); $i++){ 
      if (strpos($permitidos, substr($nombre_usuario,$i,1))===false){ 
        $error="error2"; 
      return $error;  
		 
      } 
   } 
  //echo $nombre_usuario . " es válido<br>"; 
   return $nombre_usuario; 
} 

$nombre_usuario_new=comprobar_nombre_usuario($user);

if($nombre_usuario_new=="error1"){
	 $errores= "El nombre de usuario". $nombre_usuario . " no es válido<br/> Tiene que tener entre 5 y 20 caracteres <br/>"; 
		
		
		//return false;
}
elseif($nombre_usuario_new=="error2"){
	 $errores= "El nombre de usuario". $nombre_usuario . " no es válido, esta mal formado, no puede tener espacios en blanco, acentos, ñ ...<br/>";     
	   
}
else{

if($pass!=$pass2){
	$errores="Hay un error, los password eran distintos.";
}else{

				
				/// miramos si ese nombre de usuario se esta usando
				$result_usu = mysqli_query($con,"SELECT usuario FROM $tbn9 WHERE usuario='$nombre_usuario_new'") or die("No se pudo realizar la consulta a la Base de datos");
					$quants_usu=mysqli_num_rows($result_usu);
					
					if ($quants_usu == ""){

						$passw = md5($pass);
						$nivel_usuario=2; // nivel de administrador
						$nivel_acceso=0; //maximo nivel de administrador
						$tipo_votante=1;//usuario tipo afiliado
				
							$insql = "insert into $tbn9 (pass,id_provincia,usuario,correo_usuario,tipo_votante,nivel_acceso,nivel_usuario,nombre_usuario,id_ccaa,nif,id_municipio) values (  \"$passw\",  \"$provincia\", \"$nombre_usuario_new\", \"$email\", \"$tipo_votante\", \"$nivel_acceso\", \"$nivel_usuario\", \"$username\",\"$id_ccaa\",\"$nif\", \" $id_municipio\")";	
							mysqli_query($con,$insql);
								if(mysqli_error($con) ){
									$errores= "Error al incluir datos: " . mysqli_error($con);
									}else{
									$correcto= " Sus datos han sido incluidos <br/> Ya puede acceder al sistema de votaciones";
									}
									
					}else{
				$errores= "Este nombre de usuario ya se esta usando, cambielo, por favor";						
					}					
				
}
}
if($errores!=""){
	echo "ERROR#".$errores." ";
}else{
	echo "OK#<div class=\"alert alert-success\"> 
             <a class=\"close\" data-dismiss=\"alert\">x</a>".$correcto."
			
			Se han guardado los datos correctamente
             </div>";
}

?>