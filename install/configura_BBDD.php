
<?php 
include("../basicos_php/basico.php") ;
include("../basicos_php/modifika_config.php") ;

 
$dbname=fn_filtro_nodb($_POST['dbname']);
$username=fn_filtro_nodb($_POST['username']);
$pass=fn_filtro_nodb($_POST['pass']);
$dbhost=fn_filtro_nodb($_POST['dbhost']);
$prefijo=fn_filtro_nodb($_POST['prefijo']);

$prefijo= strtolower($prefijo); //lo convertimos en minusculas
function comprobar_prefijo($nombre_usuario){ 
   
   //compruebo que los caracteres sean los permitidos 
   $permitidos = "abcdefghijklmnopqrstuvwxyz0123456789-_"; 
   for ($i=0; $i<strlen($nombre_usuario); $i++){ 
      if (strpos($permitidos, substr($nombre_usuario,$i,1))===false){ 
        $error="error"; 
      return $error;  
		 
      } 
   } 
  //echo $nombre_usuario . " es válido<br>"; 
   return $nombre_usuario; 
} 

$prefijo=comprobar_prefijo($prefijo);
if($prefijo=="error"){
	echo "ERROR#<div class=\"alert alert-danger\">Hay un error en la extension, solo estan permitidos estos caracteres <strong> abcdefghijklmnopqrstuvwxyz0123456789-_</strong></div>";
}else{

#Conectando con MySQL
$conexion=mysqli_connect($dbhost, $username, $pass);

if(mysqli_connect_errno()){
echo "ERRROR#<div class=\"alert alert-danger\"> Lo sentimos, no se ha podido conectar con la base de datos MySQL <br/> 
<i>Error de conexión número:</i> ". mysqli_connect_errno()." <i>equivalente a:</i> ". mysqli_connect_error()."</div> ";
}else {



#Conectando con la base de datos
            $dbconnect = mysqli_select_db($conexion,"$dbname");
				if ($dbconnect==0)
				echo "ERROR# <div class=\"alert alert-danger\">Se logró conectar con MySQL
				<br>pero no se ha podido conectar con la base datos: $dbname </div>";
				else
				{
				$mensaje.="
				Usuario <strong>$username </strong> correcto <br/>
				<strong>Password</strong> de la base de datos OK <br/>
				Host <strong> $dbhost </strong> correcto <br/>
				Base de datos: <strong>$dbname</strong>  conexion correcta<br />
				Prefijo de las tablas <strong> $prefijo </strong>";
				$estado_error=false;
				if (!mysqli_set_charset($conexion, "utf8")) { //establecemos como utf8 los carateres de la base de datos
					echo "ERROR#  Error cargando el conjunto de caracteres utf8: %s\n" . mysqli_error($conexion);
					$estado_error=true;
				} else {
					
				

#Probando si existe la tabla de usuarios
				
					$tabla=$prefijo."votantes";
					$idresult=mysqli_query ($conexion,"SHOW TABLES LIKE $tabla");
					if ($idresult==0) {
						
						// modificamos los datos de configuracion de la bbdd en el archivo config
						
						$file="../config/config.inc.php"; //archivo que hay que modificar
						if(file_exists($file))
						{
						  $estado_error=true;
						  $mensaje3=" ¡¡ Ya existe un archivo de configuración!!, no se puede continuar con el proceso, proceda manualmente o eliminelo";
						}else{  					
						
						///// Si tenemos los datos vamos a escribir los datos en el archivo config
						/////// creamos los docigos aleatorios de las claves de encriptacion y de las sesiones
						
						$str = "ABCDEFGHIJKLMNOPQRSTUVWXYZabcdefghijklmnopqrstuvwxyz";
						$cad1 = "";
						$cad2 = "";
						$cad3 = "";
						$cad4 = "";
						for($i=0;$i<12;$i++) {
						$cad1 .= substr($str,rand(0,62),1);
						$cad2 .= substr($str,rand(0,62),1);
						$cad3 .= substr($str,rand(0,62),1);
						$cad4 .= substr($str,rand(0,62),1);
						}
						
					
							
							//miramos la url que se usa de base en la instalacion para meeterla luego en nuestars variables
							
								
								$host= $_SERVER["HTTP_HOST"];
								$url= $_SERVER["REQUEST_URI"];
								//$url = substr($url, 0, -1); //quitamos la ultima barra de la url
								
								$url = explode("/",$url);
								$url2 = array_splice($url,-2); //quitamos los dos ultimos elementos del array
								
								$url =implode("/", $url);
								$laUrl="http://" . $host . $url;

								if(isset($_SERVER['HTTPS'])) {
								    if ($_SERVER['HTTPS'] == "on") {
								        $laUrl="https://" . $host . $url;
								    }
								}
								
							

							
							$fichero="cabecera_config.php"; //fichero que nos sirve de base para crear el nuevo
							
							if (!copy($fichero, $file)) {
								echo "Error al copiar $fichero...\n";
							}
							
							#Abrimos el fichero en modo de escritura 

								$fh = fopen($file, 'r+');
								
								
								#preparamos el streng con los datos
								$string1 = "
\$hostu = \"".$username."\";                              // Usario de la BBDD 
\$hostp = \"".$pass."\";                          // Contraseña de la BBDD 
\$dbn = \"".$dbname."\";              // Nombre de la BBDD 
\$host = \"".$dbhost."\";                            // localhost de la BBDD               		  					 				 
\$extension = \"".$prefijo."\";                             // prefijo de las tablas de la base de datos	\n							


\$url_vot=\"".$laUrl."\";      // Url donde instalamos nuestra aplicación


##################################################################################################################################
###############     variables del sistema de subida y/o redimension de imagenes de los candidatos y roots       ###############
###############                    Ojo, darle permisos de escritura en el servidor si corre en Linux               ###############
##################################################################################################################################

\$upload_cat = \"../upload_pic\";          //carpeta donde se guardan las imagenes de los candidatos
\$upload_user = \"../upload_user\";        //carpeta donde se guardan las imagenes de los roots
\$baseUrl = \$url_vot.\"/userfile/\";      //   carpeta donde se guardan las imagenes y archivos de gestor ckfinder

##################################################################################################################################
###############                                        configuración de carpetas                                   ###############
###############                    Ojo, darle permisos de escritura en el servidor si corre en Linux               ###############
##################################################################################################################################

\$FilePath=\"../data_vut/\";                  //   carpeta donde se generan los archivos del vut
\$path_bakup_bbdd=\"backup\";                 // Carpeta donde se guardan los back-up de la bbdd


###################################################################################################################################
#############                       Todo este grupo de variables no tienen porque ser modificadas.                    #############
#############                                 hacerlo solo si se tiene conocimientos                                  #############
###################################################################################################################################

\$b_debugmode = 0; // 0 || 1  Forma de errores cuando hay problemas con la base de datos



##################################################################################################################################
#####################                               Otras viariables del sistema                             #####################
##################################################################################################################################


\$usuarios_sesion=\"".$cad1."\"; // nombre de la sesion 
\$usuarios_sesion2=\"".$cad2."\"; // nombre de la sesion de los interventores
\$clave_encriptacion=\"".$cad3."\";// 
\$clave_encriptacion2=\"".$cad4."\";							


?>";
								 
								 
								
								
								fseek($fh, -3, SEEK_END); // nos vamos 3 caraceres antes para quitar el cierre de php
								fwrite($fh, $string1)or die("Could not write file!");
								/*if (fwrite($fh, $string1) === FALSE){ //escribimos en el archivo
								echo "No se puede escribir en el archivo ($file)";
								exit;
								}*/
								fclose($fh);  //  Cerramos el fichero 
														
					//	metemos las tablas	en la base de datos				
						for($i=0;$i<19;$i++) {
						 include("tablas_bbdd.php");

							//	$sql = mysqli_real_escape_string($conexion, $sql); //escapamos los datos por si acaso
									mysqli_query($conexion, $sql[$i]);
										if(mysqli_error($conexion) ){
										$mensaje1= "Error creating table: " . mysqli_error($conexion);		
										$estado_error=true;
										} else {
											$estado_error=false;
				
											//echo "Las tabla $nombre_tabla_1 ha sido correctamente creada";
										}	
								}	
								
								//incluimos los datos de los municipios
								include("municipios.php");
							    $sql = array(); 
								foreach( $municipios as $posicion=>$row ) {
									$sql[] = '('.$row['id_municipio'].', '.$row['id_provincia'].', '.$row['cod_municipio'].', '.$row['DC'].', "'.mysqli_real_escape_string($conexion,$row['nombre']).'")';
								}
								mysqli_query($conexion,'INSERT INTO '.$prefijo.'municipios (id_municipio, id_provincia,cod_municipio,DC,nombre) VALUES '.implode(',', $sql));
								if(mysqli_error($conexion) ){
										$mensaje2= "Error al incluir datos en la Base de datos: " . mysqli_error($conexion);		
										$estado_error=true;
										} else {
											$estado_error=false;
										//	echo "Los datos de la base de datos han sido correctamente incluidos";
										}
								///// metemos el resto de los datos de la ccaa
								include("datos_bbdd_array.php");
								
								$sql1 = array(); 
								foreach( $ccaa as $posicion=>$row ) {
									$sql1[] = '('.$row['ID'].', "'.mysqli_real_escape_string($conexion,$row['ccaa']).'", '.$row['especial'].')';
								}
								mysqli_query($conexion,'INSERT INTO '.$prefijo.'ccaa (ID, ccaa,especial) VALUES '.implode(',', $sql1));
								if(mysqli_error($conexion) ){
										$mensaje2= "Error al incluir datos en la Base de datos: " . mysqli_error($conexion);		
										$estado_error=true;
										} else {
											$estado_error=false;
										//	echo "Los datos de la base de datos han sido correctamente incluidos";
										}
								///// metemos el resto de los datos de  nivel_usuario
								
								
								$sql2 = array(); 
								foreach( $nivel_usuario as $posicion=>$row ) {
									$sql2[] = '('.$row['id'].',"'.mysqli_real_escape_string($conexion,$row['nivel_usuario']).'" )';
								}
								mysqli_query($conexion,'INSERT INTO '.$prefijo.'nivel_usuario (id, nivel_usuario) VALUES '.implode(',', $sql2));
								if(mysqli_error($conexion) ){
										$mensaje2= "Error al incluir datos en la Base de datos: " . mysqli_error($conexion);		
										$estado_error=true;
										} else {
											$estado_error=false;
											
										//	echo "Los datos de la base de datos han sido correctamente incluidos";
										}
								
								///metemos los datos de las provincias
								$sql3 = array(); 
								foreach( $provincia as $posicion=>$row ) {
									$sql3[] = '('.$row['ID'].', "'.mysqli_real_escape_string($conexion,$row['provincia']).'", '.$row['especial'].', '.$row['id_ccaa'].')';
								}
								mysqli_query($conexion,'INSERT INTO '.$prefijo.'provincia (ID, provincia,especial,id_ccaa) VALUES '.implode(',', $sql3));
								if(mysqli_error($conexion) ){
										$mensaje2= "Error al incluir datos en la Base de datos: " . mysqli_error($conexion);		
										$estado_error=true;
										} else {
											$estado_error=false;
										//	echo "Los datos de la base de datos han sido correctamente incluidos";
										}
										
										/*
									for($i=0;$i<2;$i++) {
									 include("datos_bbdd.php");
							
						 			mysqli_query($conexion, $sql[$i]);
										if(mysqli_error($conexion) ){
										$mensaje2= "Error al incluir datos en la Base de datos: " . mysqli_error($conexion);		
										$estado_error=true;
										} else {
											$estado_error=false;
										//	echo "Los datos de la base de datos han sido correctamente incluidos";
										}

									}	
									*/
						$mensaje.= " <br/>Las tablas de la base de datos han sido correctamente incluidas";
					    $estado_error=false;
						
						}
						
					}else
					{
							
						echo "ERROR# <div class=\"alert alert-danger\"> Se ha podido conectar con la BBDD pero parece que hay una instalacion con este prefijo</div>";
							
							
					 }
				}
					 if ($estado_error==true){
						 unlink($file); // como hay error borramos el fichero de configuración
					echo "ERROR#<div class=\"alert alert-info\"> 
				$mensaje
				 </div>	<div class=\"alert alert-danger\">$mensaje1 $mensaje2 $mensaje3</div> ";
					 }
					 if ($estado_error==false){
					              echo "OK# 
				<div class=\"alert alert-info\"> 
				$mensaje 
				 </div>";	 
						 
					 }

	
			mysqli_close($conexion);  
	        }
        }
}

?>
