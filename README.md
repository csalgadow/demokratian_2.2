# README #


## ATENCIÓN, este repositorio esta desactualizado y desasistido##
## La versión 4.5.x la puedes descargar en https://bitbucket.org/csalgadow/demokratian-v4/src/master/ ## 




Los requerimientos necesarios para poner en marcha la plataforma de votaciones DEMOKRATIAN son muy sencillos. Necesita un servidor apache con php5 y una base de datos mysql. Dependiendo de si tiene muchos miles de inscritos el servidor tendrá que ser más o menos potente

### ¿Para que es este repositorio? ###

* DEMOKRATIAN , una plataforma desarrollada por [Carlos Salgado Werner](http://carlos-salgado.es) para poder implementar una forma de decisión horizontal en su organización. Todos los miembros registrados podrán votar de una forma sencilla con la seguridad de que su voto es secreto.
Permite múltiples tipos de votación, actualmente dispone de 4 tipos o formas de votación. De esta forma el administrador puede elegir entre abrir un debate, realizar una encuesta, hacer una votación con voto ponderado, o realizar una votación con recuento por VUT.
DEMOKRATIAN está especialmente diseñada para que sea muy fácil de usar, tanto por el usuario final, el votante, como por los distintos niveles de administradores. De esta forma no tendrá que preocuparse más que de realizar las preguntas o añadir sus candidatos para sus votaciones.
* Version [demokratian_2.2] (https://bitbucket.org/csalgadow/demokratian_2.2/downloads)
* Web de [demokratian.org](http://demokratian.org)


### Como configurar la aplicación ###

Para instalar la aplicación, suba todos los archivos a su servidor.
Hay 4 carpetas que tienen que tener permiso de escritura para el usuario con el que corre apache: config, upload_user, upload_pic, data_vut (usamos chown $USUARIO.$GRUPO y chmod 0700 para ello). Estas carpetas puede cambiarlas el nombre posteriormente mediante el archivo de configuración.

La aplicación tiene un sistema de instalación automático, para ello debe de ir mediante su navegador a la carpeta install (midominio.org/install)
Para realizar la instalación necesita conocer los datos de la base de datos de su servidor asi como los datos de configuración del servidor de correo.
Esa información se usa para configurar el archivo config.inc.php que está en la carpeta config. Si por cualquier motivo la creación automática no funcionara, o posteriormente quiere hacer una modificación que no pueda realizar mediante el panel de administración de la aplicación, siempre podrá modificar ese archivo a mano.


### Como contribuir ###

* Writing tests
* Code review
* Other guidelines

### Contactar ###

Para contactar puede hacerlo mediante el correo info@demokratian.org
Aun no tenemos equipo de contribuidores así que  no podemos darle más formas de contacto.
