<?php
###############################################################################################################################################################
###############################################################################################################################################################
###                                                                                                                                                         ###
###                                                                DEMOKRATIAN                                                                              ###
###                                                         http://demokratian.org                                                                          ###
###                                                  2015 CARLOS SALGADO WERNER (http://carlos-salgado.es)                                                  ###
###                                         Este programa ha sido creado por Carlos Salgado Werner                                                          ###
###                                                                                                                                                         ###
### Este programa es software libre. Puede redistribuirlo y/o modificarlo bajo los términos de la Licencia Pública General de GNU según es publicada por la ###
### Free Software Foundation, bien de la versión 3 de dicha Licencia o bien de cualquier versión posterior.                                                 ###
### Este programa se distribuye con la esperanza de que sea útil, pero SIN NINGUNA GARANTÍA, incluso sin la garantía MERCANTIL implícita o sin garantizar   ###
### la CONVENIENCIA PARA UN PROPÓSITO PARTICULAR. Véase la Licencia Pública General de GNU para más detalles.                                               ###
### Debería haber recibido una copia de la Licencia Pública General junto con este programa. Si no ha sido así, puede encontrarla en                        ###
### http://www.gnu.org/licenses/gpl-3.0.html                                                                                                                ###
### Si quieres participar en la mejora de este software ,eres libre de hacerlo,                                                                             ###
### También puedes contactar con migo en el correo info@demokratian.org para trabajar en el desarrollo de forma colaborativa                                ###
###                                                                                                                                                         ###
###                                          Por favor, no elimines este aviso de licencia                                                                  ###
###                                                                                                                                                         ###
###############################################################################################################################################################
###############################################################################################################################################################
//require_once("../inc_web/conexion.php");?>
<!DOCTYPE html>
<html>
<head>
  <meta http-equiv="content-type" content="text/html; charset=UTF-8">
  <title>Ayuda</title>  
</head>
<body>
  
        <div class="modal-content">
            <div class="modal-header">
                <a class="close" data-dismiss="modal" >x</a>
                            <!--    <button type="button" class="close" data-dismiss="modal" aria-hidden="true">&times;</button>-->
                               <h4 class="modal-title">Ayuda a la administración de la plataforma de votaciones 2.x</h4>
                 
            </div>            <!-- /modal-header -->
            <div class="modal-body">
              <p> </p>
             <p><strong>Gestion administración</strong></p>
             <ul>
               <li>Gestión usuarios administración
                 <ul>
                   <li>Como gestionar los administradores</li>
                   <li>Tipos de administradores</li>
                   <li>Votaciones por usuario</li>
                   <li>Votaciones por usuario</li>
                 </ul>
               </li>
               <li>Gestion notificaciones provincias</li>
               <li>Gestion páginas</li>
               <li>Datos de la configuración por defectos de la web</li>
             </ul>
             
             <p><strong>Las votaciones</strong></p>
             <ul>
               <li>Como crear una votación
                 <ul>
                   <li>Tipos de votaciones, Primarias ,VUT, Encuesta, Debate.</li>
                   <li>Control-verificación (comprobación de voto e interventores)</li>
                   <li>Interventores-apoderados de mesa</li>
                 </ul>
               </li>
               <li>Gestion / modificación de votaciones</li>
               <li>Buscador candidatos / opciones</li>
               <li>Resultados /censos por votación</li>
               <li>Voto presencial/ congreso</li>
             </ul>
             <p> </p>
             <p><strong>Mis votaciones</strong></p>
             <ul>
               <li>Gestion / modificación de votaciones</li>
               <li>RESULTADOS censos por votación</li>
               <li>Voto presencial/congreso             </li>
             </ul>
             <p><strong>Cómo gestionamos los usuarios. Los </strong><strong>Censos</strong></p>
             <ul>
               <li>Buscar / modificar votantes</li>
               <li>Incluir un votante</li>
               <li>Añadir votantes de foma masiva</li>
               <li>Bloquear / desbloquear votantes</li>
               <li>Bajas de votantes de foma masiva</li>
               <li>Actualizar municipios de foma masiva</li>
               <li>Buscar municipios de foma masiva</li>
             </ul>
             <p> </p>
             <p><strong>Qué son las asambleas y cómo se gestionan</strong></p>
             <ul>
               <li>Crear asambleas</li>
               <li>Modificar Asambleas</li>
               <li>Gestionar usuarios.</li>
             </ul>
             <p> </p>
             
             <blockquote>
               <blockquote>
                 <blockquote>
                   <blockquote>
                     <blockquote>
                       <blockquote>
                         <p>Por: Carlos Salgado</p>
                       </blockquote>
                     </blockquote>
                   </blockquote>
                 </blockquote>
               </blockquote>
             </blockquote>
             <p>&nbsp;</p>
             <p><strong>Introducción</strong></p>
             <p>El sistema de votaciones tiene dos zonas diferenciadas de actuación, una como usuario, y otra como administrador.</p>
             <p>A la hora de diseñar la aplicación, se ha realizado pensando fundamentalmente en</p>
             <ul>
               <li>Una fácil gestión de votantes en la que se pueda tramitar por parte del administrador los censos de votantes y que además puedan votar, en función del tipo de votación,, afiliados, simpatizantes verificados y simpatizantes</li>
               <li>Que se puedan realizar varios tipos de votación</li>
               <li>Posibilidad de múltiples tipos de circunscripciones</li>
               <li>No trazabilidad del voto</li>
             </ul>
             <p>Si eres administrador podrás ver un menú en la parte superior de la web, este menú se distingue fácilmente ya que es de color negro.</p>
             <p>El menú del lateral izquierdo es como el de cualquier otro usuario, y mediante el  podrás ejercer tus derecho a voto y participar como un usuario mas.</p>
             <p>Existen varios niveles de administrador y en función de estos niveles podremos realizar unas acciones u otras</p>
             <p><strong>Como gestionar los administradores</strong></p>
             <p>Si su usuario  tiene asignados permisos para gestionar  y crear administradores, en el menú de administración le aparecerá la opción &ldquo;gestión administración&rdquo; y dentro del desplegable esta la opción &ldquo;gestión usuarios administración&rdquo;. Al pinchar esta opcion le aparecerá una pantalla en la que por motivos de seguridad, deberá volver a poner su usuario y su clave de acceso. Al hacer click en entrar, accedera a un buscador que le permitirá buscar al usuario al que quiere dar los permisos. El buscador nos presentara un listado de usuario que se ajustan al criterio de búsqueda, en cada usuario vemos varias opciones.</p>
             <p><img src="../temas/demokratia/imagenes/imagenes_ayuda/captura1.gif" alt="002" width="720" height="343" title=""></p>
             <p>En el lado de la izquierda , al pinchar sobre la imagen (1) se nos abrirá una ventana con todos los datos del usuario</p>
             <p>Si el usuario es un administrador provincial, o de grupo, tendremos  una opcion que dice &ldquo;asignar provincias o grupos&rdquo; (2) , al pinchar sobre el, se nos abrira una ventana que nos permitirá asignar provincias y/o grupos en función del tipo de administrador que sea.</p>
             <p>Por ultimo tenemos la opcion &quot;modificar&quot; (3) que nos da acceso a la pantalla para modificar el tipo de usuario y asignarle tareas administrativas. Esta misma pantalla nos permitiria quitarle los permisos a un administrador y convertirlo en votante.             </p>
             <p>&nbsp;</p>
             <p> </p>
             <p><strong>Tipos de administradores</strong></p>
             <p>Existen los siguientes tipos de administradores</p>
             <ul>
               <li>Administrador</li>
               <li>Admin CCAA</li>
               <li>Admin Provincia</li>
               <li>Admin Grupo Provincial</li>
               <li>Admin Grupo Estatal             </li>
             </ul>
             <p>El sistema funciona como un sistema de cascada , de forma que el usuario mas alto tiene permisos para su administración, así como las de los niveles inferiores, de forma que los administradores estatales podrán crear cualquier tipo de votación, los autonómicos, de su CCAA , votaciones las provincias de su CCAA, de los grupos provinciales. Tan solo se sale de esta norma la administración de Grupos Estatales que sólo tendrán acceso a ella , además de ese perfil, el perfil del administrador general.</p>
             <p>Además de estos niveles, tenemos un segundo grupo de niveles &ldquo;numéricos&rdquo; que se cada administrador tendrá asignado y que permiten según se tenga uno u otro asignado.:</p>
             <ul>
               <li>7 - Nivel minimo para poder añadir imágenes (no operativo aun)</li>
               <li>5 - Permite crear grupos locales</li>
               <li>2 - Permite crear administradores locales y modificar direccion de notificación de correo</li>
               <li>1 - Permite modificar censos</li>
               <li>0 - Todos los permisos, incluido crear administradores generales</li>
             </ul>
             <p>nota : Los administradores de grupos sólo podrán  tener niveles superiores al 6.</p>
             <p>De la misma forma que en el caso anterior, un administrador tendra asignado los permisos de su número, así como los permisos de todos los números superiores.</p>
             <p>Asignele los permisos que le correspondan, (Si lo desea puede hacer que el sistema notifique por correo los cambios al usuario)  y haga click en actualizar.</p>
             <p>Si el tipo de administrador que ha cogido es provincial y/o de grupo, le aparecerá un mensaje  en la parte superior para, si lo desea, asignarle provincias de su CCAA o grupos</p>
             <h6>Los números que faltan se irán asignando en futuros desarrollos según necesidades</h6>
             <p> </p>
             <p><strong>Votaciones por usuario</strong></p>
             <p>Para facilitar el control de administradores y de la aplicación , si tiene permisos de super administrador, nivel &ldquo;0&rdquo;  en la pestaña de &ldquo;gestion de la administracion&rdquo; le aparecera otra opcion, &ldquo;votaciones por usuario&rdquo; que le permitirá buscar usuarios y acceder directamente a la gestión de sus votaciones.             </p>
             <p>Existe también la posibilidad de realizar esta gestión de todas las votaciones de su nivel de autorización mediante las opciones &ldquo;gestión general de votaciones&rdquo; que se detalla en otro punto</p>
             <p> </p>
             <p><strong>Gestion notificaciones provincias</strong></p>
             <p>Solo accesible para usuarios con permiso 2 o inferior, permite incluir las direcciones de correo donde se notifican las incidencias de las distintas provincias</p>
             <p> </p>
             <p><strong>Datos de la configuración por defecto de la web</strong></p>
             <p>Permite cambiar alguno de los parámetros fijos de la web y que se configuran el el archivo general de configuración, solo se puede acceder con permisos de superusuario &quot;0&quot;.</p>
             <p> </p>
             <p><strong>Las votaciones</strong></p>
             <p><strong>Como crear una votación</strong></p>
             <p>Crear una nueva votación es extremadamente fácil, en el menú de administración , en el desplegable que aparece en la opción &ldquo;gestión general de votaciones&rdquo;, la primera opción que le aparece es &ldquo;crear una votación&rdquo;.</p>
             <p>Al acceder le aparecerán una serie de opciones.</p>
             <ul>
               <li><strong>Nombre de la votación.</strong> Aquí, como su título indica, pondremos el nombre e la votación</li>
               <li><strong>Demarcación; </strong> ¡¡ATENCION!! Este grupo de opciones variará según el nivel de administrador que tenga. Podrá elegir la zona o grupo en el que quiere crear la votación.  En función de la demarcación escogida, le aparecerá un desplegable (salvo en demarcación estatal) donde podrá elegir la CCAA (si tiene permisos para actuar en más de una), o provincias o grupos que tengan asignados.Tenga en cuenta que en función de esta elección podrán votar unos usuarios u otros  </li>
             </ul>
             <ul>
               <li><strong>Fecha de inicio / fecha final </strong>.Corresponde a las fechas entre las cuales el usuario podrá votar. La votación se activará o desactivará automáticamente entre estas fechas.</li>
               <li><strong>Tipo de votación.</strong> Este grupo de opciones no podrá modificarse una vez creada la votación. Tenemos las siguientes opciones: Primarias , VUT, Encuesta, Debate:
                 <ul>
                   <li><strong>PRIMARIAS</strong> se podrá elegir las opciones   más votadas de una lista por cómputo ponderado.<br>
                     Existen dos tipos de ponderación               
                     <ul>
                       <li> BORDA El votante podrá ordenar una lista de opciones donde en función del orden,  a cada posición de la ordenación le es atribuida una puntuación: 1 punto para el último clasificado, 2 puntos para el penúltimo, 3 para el antepenúltimo etc</li>
                       <li> DOWDALL El votante podrá ordenar una lista de opciones donde en función del orden, el primero tendrá 1 punto, el segundo 0.5 el tercero 0.25 y sucesivamente (1/n)</li>
                     </ul>
                   </li>
                   <li><strong>VUT</strong> (voto único transferible), sistema de recuento sobre el que se puede encontrar información aquí, por ejemplo: <a href="http://www.google.com/url?q=http%3A%2F%2Fequomunidad.org%2Fes%2Fel-voto-unico-transferible-%25E2%2580%2593-un-sistema-electoral-para-equo&sa=D&sntz=1&usg=AFQjCNE13SIoqNXnXPhPyzsyb9lIjh7qXg">http://equomunidad.org/es/el-voto-unico-transferible-%E2%80%93-un-sistema-electoral-para-equo</a>)</li>
                   <li><strong>ENCUESTA; </strong>permite elegir una opción -o más entre, varias. es un voto que no tiene ningún tipo de ponderación.</li>
                   <li><strong>EL DEBATE: </strong>Se trata un nuevo tipo de opción incluida en esta nueva versión. , ahora se podrá abrir un DEBATE. Este nuevo tipo permite mandar comentarios y debatir sobre un tema y si se necesita, votar una o varias preguntas y este voto variarlo a medida que transcurre el debate si cambiamos de opinión. Este sistema no asegura la no trazabilidad del voto, es decir, si alguien accede a la bbdd podría llegar a ver que ha votado quien y tampoco tiene el sistema de envío de correos a interventores.</li>
                 </ul>
               </li>
               </ul>
             </blockquote>
             <ul>
               <li><strong>En &ldquo;Tipo de votante&rdquo;</strong>, se seleccionan los votantes que pueden participar en esta elección: &ldquo;Solo socios&rdquo; , &ldquo;Socios y simpatizantes verificados&rdquo; &ldquo;Socios y simpatizantes (verificados y no verificados)&rdquo;.</li>
               <li><strong>Estado:</strong> Nos permite dos opciones Activado - Desactivado e indica si la votación esta &ldquo;visible&rdquo; para los votantes o no. ¡¡OJO!! esta opción &ldquo;puede&rdquo; a la fecha, de tal forma que si esta desactivada, aunque este en fecha de votación no se verá, y por tanto no se podrá votar.</li>
               <li><strong>Numero de opciones</strong> que se pueden votar. Indica el numero de opciones que se pueden votar y debe de hacerse mediante un valor numérico. En el tipo encuesta, si no hay limite ponga un "0"</li>
               <li><strong>Seguridad de control de voto.</strong> Se trata de un sistema de control-verificacion de lo votado. El sistema  sigue manteniendo la casi imposible  trazabilidad del voto, es decir, el voto es secreto y aunque alguien accediera a la base de datos seria muy difícil  conocer que ha votado quien.(imposible  decir &ldquo;imposible&rdquo;,  ya que hay cracks que consigue hasta entrar en los bancos…. ).  tenemos las siguientes opciones. Estas opciones no estaran disponibles si escoge una votacion tipo debate.
                 <ul>
                   <li><em>Sin trazabilidad ni interventores</em> No activa ningn metodo.</li>
                   <li><em>Comprobación de voto </em> (no funciona en VUT). El sistema permite a los usuarios ver que se mantiene su voto en la base de datos tal cual lo han emitido, este sistema no funciona con voto VUT que se implementara más tarde.</li>
                   <li><em>Con interventores. </em>El  sistema realiza el envío de un correo anónimo con cada voto a los interventores que se designen, de esta forma en caso de duda podrían recontar los votos, sería como tener una versión voto en papel que tendrían varios interventores. IMPORTANTE Si se activa esta opción tendrá, una vez creada la votación, que asignar los interventores.</li>
                   <li><em>Con comprobación de voto e interventores</em> (no funciona en VUT) :Activaria ambas opciones.</li>
                 </ul>
               </li>
             
               <li><strong>Permitir interventores especiales.</strong> Mediante esta opción permitiremos la introducción de papeletas que se hayan depositado en urna por parte de interventores. Hay que indicar el número de interventores que tienen que estar presentes y logueados en el sistema como método de control, se aconsejan 3. (actualmente solo funciona con VUT, en próxima versión estara para primarias) Para acceder posterioremente hay que hacerlo en www.nombremiweb.es/interventores</li>
               <li><strong>Resumen.</strong> Espacio para incluir un pequeño resumen de lo que trata la votación. El votante lo  vera al principio de la página de la votación, y en el desplegable del cuadro de votaciones. Se ha activado un editor  WYSIWYG para que se pueda dar formato a los textos, incluir imágenes, etc</li>
               <li><strong>Texto</strong> Espacio para incluir un texto sobre la votación, explicaciones, etc. El votante lo  vera al final de la página de la votación. Se ha activado un editor  WYSIWYG para que se pueda dar formato a los textos, incluir imágenes, etc</li>
             </ul>
             <p>Una vez que haya completado la información podrá presionar el botón de crear una nueva votación, y su votación será creada. Al crear la votación, en la parte superior de la página le aparecerá un mensaje si la votación ha sido creada, así como una invitación mediante un enlace que le indicara que para concluir la votación  puede en ese momento añadir los candidatos u opciones de la votación (1). En caso de que haya elegido el sistema de comprobación del voto mediante interventores, le aparecerá otro enlace invitandole a incluir a los interventores de esa votación (2).  En caso de que decidiera no hacerlo en este momento, tanto los candidatos-opciones , como los interventores podrán ser incluidos posteriormente (ver incluir/modificar candidatos-opciones o incluir/modificar interventores)</p>
             <p><strong>Como realizar gestión general de votaciones</strong></p>
             <p>En el desplegable del menú, en &ldquo;gestion general de censos&rdquo; tenemos ademas de la opción para crear una nueva votación tenemos otras opciones que nos permitirán realizar una gestión completa de las votaciones. Todas estas opciones nos llevan a un buscador que nos permitirá buscar las votación que queramos y el listado nos facilitará las opciones</p>
             <ul>
               <li><strong>Gestion/modificación de votaciones.</strong> Nos permitirá realizar las gestiones de modificación de las votaciones, cambiar textos, modificar número de opciones a votar, etc. Desde esta opción , también podremos activar o desactivar la votación, activar o desactivar los resultados o borrar una votación.También la gestión de candidatos/ opciones ya que podremos acceder a los candidatos/opciones  por votación para modificarlos o borrarlos, además podremos crear un nuevo candidato u opción. La última posibilidad que tenemos , es la de añadir/gestionar  interventores si es una votación que lo requiera .</li>
               <li><strong>Buscador de candidatos / opciones.</strong> Nos permite acceder a un buscador de candidatos/opciones</li>
               <li><strong>Resultados / censos por votación.</strong> Voto presencial.  Esta opción nos permite acceder al censo de los que pueden votar en cada votación en concreto, Nos permite ver el censo completo, quienes han votado ya, y quienes faltan por votar, de esta forma, si una votación tiene la posibilidad de votar también presencialmente, en la mesa de votaciones se podrá comprobar si ha votado o no, y una vez que realice el voto presencial, marcar como que el usuario ha votado para que no pueda hacerlo también virtualmente. Además, para el caso de congresos, asambleas, donde existe la opción de votar presencialmente, se podrá realizar un bloqueo masivo de los participantes para que estos voten solo en congreso/asamblea.</li>
             </ul>
             <p> </p>
             <p><strong>Que votaciones tengo yo</strong></p>
             <p><strong>Mis votaciones</strong></p>
             <ul>
               <li>Gestion / modificación de votaciones</li>
               <li>RESULTADOS censos por votación</li>
               <li>Voto presencial/congreso</li>
             </ul>
             <p>Con el fin de facilitar la gestión de las votaciones, sobre todo para los usuarios con niveles altos administrativos, existen unas opciones del menú para ver y gestionar las votaciones que ha creado uno mismo. Estas opciones se encuentran en el menú desplegable bajo el epígrafe de &ldquo;mis votaciones&rdquo; y son las mismas opciones que se encuentran en &ldquo;gestión general de votaciones, pero para sus votaciones.</p>
             <p> </p>
             <p><strong>Cómo gestionamos los usuarios.</strong></p>
             <p>Una de las opciones más importantes de la aplicacion es la gestión de usuarios. Esta aplicación está pensada para poder controlar los censos de usuarios que pueden votar, es decir, podran votar solo los usuarios que el administrador con permisos para ello haya introducido. De esta forma tenemos 3 grupos de votantes, Los socios o afiliados, los simpatizantes verificados, y el simpatizante.</p>
             <p>Solo podrán acceder a esta parte de la aplicación los administradores con nivel 1, o los superadministradores. La base de gestión de los censos es territorial, concretamente provincial.</p>
             <p>Podrá acceder a las opciones de gestion de cesos mediante el menú desplegable que se encuentra en &ldquo;gestión general de censos&rdquo;  que tiene las siguientes opciones.</p>
             <ul>
               <li><strong>Buscar/modificar votantes.</strong> Mediante esta opción se accedera a un buscador que le permitirá encontrar al votante que desee para modificar o borrarlo. Además, existe la posibilidad de bloquear un votante sin borrarlo.</li>
               <li><strong>Incluir un votante.</strong> Mediante esta opcion podra incluir un votante en el censo general</li>
               <li><strong>Añadir votantes de forma masiva.</strong> Mediante esta opción podremos incluir múltiples votantes de una sola vez . Es importante tener en cuenta que tienen que ser todos de una misma provincia, no se puede por tanto mezclar provincias, y deben de ser todos del mismo tipo, Socios o simpatizantes , etc . Además, por el sistema de gestión que se hace en EQUO a través de la aplicación civicrm, es necesario que se incluyan antes los simpatizantes verificados  a que se incluyen los  simpatizantes. Para incluir votantes de forma masiva, hay que hacerlo indicando la dirección de correo, el nombre, y el NIF, todo ello separado por comas. Solo se puede poner un votante por linea. Si un votante ya esta incluido en la base de datos, el sistema no lo volverá a incluir, e indicará en rojo que no ha sido incluido.</li>
               <li><strong>Bloquear / desbloquear votantes. </strong>Mediante un buscador, podrá encontrar el/los votantes que quiera para bloquearlos para que no puedan votar.</li>
               <li><strong>Bajas de votantes de forma masiva. </strong>Mediante un sistema similar al de la inclusión de votantes, se podrán borrar múltiples votantes o cambiarlos de tipo.</li>
               <li><strong>Actualizar municipios de forma masiva</strong>. Herramienta temporal para la transición de las versiones 1.x a las versiones 2.x para poder incluir en los censos existentes los municipios de los usuarios</li>
               <li><strong>Buscar municipios de forma masiva.</strong> Herramienta temporal para la transición de las versiones 1.x a las versiones 2.x para poder realizar de forma masiva el cambio de nombre de municipio por el del codigo del municipio.</li>
             </ul>
             <p><strong>Qué son las asambleas y cómo se gestionan</strong></p>
             <p>Además de votaciones generales, autonómicas o provinciales, se pueden crear &ldquo;asambleas&rdquo; o grupos de trabajo para realizar votaciones en ese ámbito. Estos grupos también pueden ser generales, autonómicos o provinciales, y una vez creados el votante podrá apuntarse si lo desea, de forma que un usuario de la provincia de la coruña solo verá los grupos de trabajo generales, los de la CCAA de Galicia, y los de su provincia. Estos grupos pueden ser abiertos, es decir, cualquiera puede apuntarse, o moderados, será necesaria la aprobación del administrador para que pueda participar en el.</p>
             <p>La gestión de estos grupos se hace mediante las opciones que aparecen en el menú menú desplegable de &ldquo;Asambleas&rdquo; y son:</p>
             <ul>
               <li><strong>Crear asambleas. </strong>Esta opción solo estará disponible para administradores con el nivel 5 o superior. Los administradores autonómicos podrán crear grupos en su CCAA y sus provincias, y los provinciales solo en las provincias que tengan asignadas.</li>
               <li><strong>Modificar Asambleas,</strong> Es una opcion similar a la anterior pero para realizar modificaciones.</li>
               <li><strong>Gestionar usuarios.</strong> Esta opcion permite realizar la gestion de los usuarios de los grupos de trabajo que tenga asignados, de forma que podra autorizar el acceso al grupo de los votantes que lo hayan solicitado, o bloquearlos.</li>
             </ul>
             <p> </p>
             <p><strong>Notificaciones por correo</strong></p>
             <p>Existe la posibilidad de que se puedan notificar si un votante  que tiene problemas para acceder a la aplicación, directamente a una dirección de correo que se quiera, esta notificación se hace por provincias y para cambiar las direcciones de correo se puede hacer  en &ldquo;gestion notificaciones provincias&rdquo;. Solo podra modificar la direccion de correo el usuario con los permisos correspondientes y sólo para las provincias que tenga asignadas. Si deja este campo vacío, las notificaciones se enviarán al correo genérico de la aplicación</p>
             <br>
             <p>&nbsp;</p>



		
                
                
    <!--
===========================  fin texto ayuda
-->             </div>            <!-- /modal-body -->
                       <!-- /modal-footer -->
        </div>         <!-- /modal-content -->
    
</body>
</html>