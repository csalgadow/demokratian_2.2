<?php
###############################################################################################################################################################
###############################################################################################################################################################
###                                                                                                                                                         ###
###                                                                DEMOKRATIAN                                                                              ###
###                                                         http://demokratian.org                                                                          ###
###                                                  2015 CARLOS SALGADO WERNER (http://carlos-salgado.es)                                                  ###
###                                         Este programa ha sido creado por Carlos Salgado Werner                                                          ###
###                                                                                                                                                         ###
### Este programa es software libre. Puede redistribuirlo y/o modificarlo bajo los términos de la Licencia Pública General de GNU según es publicada por la ###
### Free Software Foundation, bien de la versión 3 de dicha Licencia o bien de cualquier versión posterior.                                                 ###
### Este programa se distribuye con la esperanza de que sea útil, pero SIN NINGUNA GARANTÍA, incluso sin la garantía MERCANTIL implícita o sin garantizar   ###
### la CONVENIENCIA PARA UN PROPÓSITO PARTICULAR. Véase la Licencia Pública General de GNU para más detalles.                                               ###
### Debería haber recibido una copia de la Licencia Pública General junto con este programa. Si no ha sido así, puede encontrarla en                        ###
### http://www.gnu.org/licenses/gpl-3.0.html                                                                                                                ###
### Si quieres participar en la mejora de este software ,eres libre de hacerlo,                                                                             ###
### También puedes contactar con migo en el correo info@demokratian.org para trabajar en el desarrollo de forma colaborativa                                ###
###                                                                                                                                                         ###
###                                          Por favor, no elimines este aviso de licencia                                                                  ###
###                                                                                                                                                         ###
###############################################################################################################################################################
###############################################################################################################################################################
		header("Expires: Mon, 26 Jul 1997 05:00:00 GMT");             		
		header("Last-Modified: " . gmdate("D, d M Y H:i:s") . " GMT");		
		header("Cache-Control: no-cache, must-revalidate");           
		header("Pragma: no-cache");                                   
$nombre_fichero = 'config/config.inc.php';

if (!file_exists($nombre_fichero)) {
    echo "<p>El fichero de configuración no existe</p><p>si es la primera vez que instala la aplicación debe de ir a la carpeta de instalacion (SuSitio.org/install)</p>";
} else {
// Inicializar la sesión.
// Si está usando session_name("algo"), ¡no lo olvide ahora!
//require_once("config/config.inc.php");
require_once("config/config.inc.php");

session_name($usuarios_sesion);
session_start();

// session_name($usuarios_sesion);
// Destruir todas las variables de sesión.
$_SESSION = array();

// Si se desea destruir la sesión completamente, borre también la cookie de sesión.
// Nota: ¡Esto destruirá la sesión, y no la información de la sesión!
if (ini_get("session.use_cookies")) {
    $params = session_get_cookie_params();
    setcookie(session_name($usuarios_sesion), '', time() - 42000,
        $params["path"], $params["domain"],
        $params["secure"], $params["httponly"]
    );
}

// Finalmente, destruir la sesión.
session_destroy();
$con= @mysqli_connect("$host","$hostu","$hostp") or die ("no se puede conectar"); 
mysqli_set_charset($con, "utf8");
$db = @mysqli_select_db($con, "$dbn") or die ("no se puede acceder a la tabla"); 
?><!DOCTYPE html>
<html lang="es"><head>
    <meta charset="utf-8">
    <title><?php echo "$nombre_web"; ?></title>



    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <meta name="description" content=" ">
    <meta name="author" content=" ">
   <link rel="icon"  type="image/png"  href="temas/<?php echo "$tema_web"; ?>/imagenes/icono.png"> 
        

    
    
    <!—[if lt IE 9]>
      <script src="https://html5shim.googlecode.com/svn/trunk/html5.js"></script>
    <![endif]—>
    <link href="temas/<?php echo "$tema_web"; ?>/bootstrap/css/bootstrap.min.css" rel="stylesheet">
<!--    <link href="temas/<?php echo "$tema_web"; ?>/bootstrap/css/bootstrap-theme.min.css" rel="stylesheet">-->
    <link href="temas/<?php echo "$tema_web"; ?>/estilo.css" rel="stylesheet">
    <!--<link href="temas/<?php echo "$tema_web"; ?>/estilo_login.css" rel="stylesheet">-->
  </head>
  
  <body>

 <div class="container">
 
    <!-- cabecera
    ================================================== -->
      <div class="page-header">
      <p>&nbsp;</p>
      <img src="temas/<?php echo "$tema_web"; ?>/imagenes/cabecera_votaciones.jpg" class="img-responsive" alt="Logo <?php echo "$nombre_web"; ?>">
        
      </div>
      
    <!-- END cabecera
    ================================================== -->
   <div class="row">    
     <div class="col-lg-6">
            <div class="jumbotron">
              <h2>Bienvenido al sistema de votaciones <?php echo "$nombre_web"; ?></h2>
              <p>Si es la primera vez que accedes deberás generar tu clave y usuario usando la dirección de correo electrónico con la que estes registrado en la base de datos.</p>
             <p><a data-toggle="modal" href="#myModal"  class="btn btn-primary btn-lg">Registrarme</a> </p>
			 </div>
        </div>
  
 
      
     <div class="col-lg-6">   
	<div class="well">
      <form action="votacion/inicio.php" method="post"  class="form-horizontal" role="form" >
        <h3 class="form-signin-heading">Tienes que identificarte para acceder</h3>
         <?php 
		  include ("inc_web/ms_error.php");
            if (isset($_GET['error_login'])){
                $error=$_GET['error_login'];
				if (isset($_GET['tx'])){
                $tx="<br/>".$_GET['tx'];}
			?>	
			 <div class="alert alert-danger"> 
             <a class="close" data-dismiss="alert">x</a>
             
             Error: <?php echo $error_login_ms[$error]; ?><?php echo $tx; ?> <br/>
             <a data-toggle="modal" href="#myModal" >Si no estas registrado puedes hacerlo</a> <br/>
             <a data-toggle="modal" href="#myModal" >No recuerdo mi contraseña</a>
             </div>

			 <?php     }
		?>
		<div class="form-group">
      <label for="user" class="col-sm-4 control-label"><?=_('Usuario');?></label>
      <div class="col-sm-6">
        <input type="text" id="user"   name="user" class="form-control" placeholder="Usuario" required autofocus/>
        </div>
        </div>
        <div class="form-group">
      <label for="user" class="col-sm-4 control-label"><?=_('Password');?></label>
      <div class="col-sm-6">
          <input type="password" id="pass"  name="pass" class="form-control" placeholder="Password" required />
 		</div>
        </div>
        <div class="form-group">
      <div class="col-sm-offset-4 col-sm-6">
        <button class="btn btn-ttc btn-lg btn-primary btn-block" type="submit">Entrar</button>
      </div>
      </div>
      </form>
      </div>
      <div class="well">
     <a data-toggle="modal" href="#myModal" >Puedes registrarte o recuperar tu contraseña</a> </p>
		</div>
        
       <div class="alert alert-info">
     <a href="http://demokratian.org/" >Que es demokratian</a> </p>
		</div> 
    </div> 
    
    </div>
   <!--
   ================================= ventana modal
   -->    
 <div class="modal fade" id="myModal">
        <div class="modal-dialog">
                <div class="modal-content">
                        <div class="modal-header">
                        <a class="close" data-dismiss="modal" >x</a>
                            <!--    <button type="button" class="close" data-dismiss="modal" aria-hidden="true">&times;</button>-->
                               <h4 class="modal-title">Complete para registrarse o recuperar su contraseña</h4>
                        </div>
                        
              <div class="modal-body">
              <fieldset>
				
                <form name="sentMessage" class="well" id="contactForm" >                
               <!-- <form name="sentMessage" class="well" id="contactForm"  novalidate>-->
	       <!--<legend>Contact me</legend>-->
		 <div class="control-group">
                    <div class="controls">
			<input type="text" class="form-control" 
			   	   placeholder="Su nombre" id="name" required data-validation-required-message="Por favor, ponga su nombre" />
			  <p class="help-block"></p>
		   </div>
	         </div> 	
                <div class="control-group">
                  <div class="controls">
			<input type="email" class="form-control" placeholder="Su correo electronico" id="email" required  data-validation-required-message="Por favor, ponga su correo electronico" />
		</div>
	    </div> 	
        
        
        

               <div class="control-group">
                <span class="help-block">Seleccione la provincia donde esta censado:</span>
                            <?php 
				  

// listar para meter en una lista del tipo enlace
$activo=0;
$tbn8=$extension."provincia";
	$options = "select DISTINCT ID,provincia from $tbn8 where especial ='$especial' order by id  ";
	$resulta = mysqli_query( $con, $options) or die("error: ".mysqli_error());
	while( $listrows = mysqli_fetch_array($resulta) ){ 
	$name= $listrows[provincia];
	$id_pro=$listrows[ID];
	$lista .="<option value=\"$id_pro\"> $name</option>";
	
	
}
				  
				  ?>
                    
	  <div class="form-group">
                    <select class="form-control"  name="provincia" id="provincia" >
                     <!-- <option value=""> Escoje una provincia</option>-->
         				 <?php echo "$lista";?>
                    </select>
                 </div>
                
                
                
               </div> 		 
               
               
	     <div id="success"> </div> <!-- mensajes -->
	    <button type="submit" class="btn btn-primary pull-right">Enviar</button><br />
          </form>
                
                
                
                
</fieldset>
			</div>
            
			
             
             </div>
        </div>
</div>
<!--
===========================  fin ventana modal
-->
<!--
========================== segunda ventana modal 
-->


 <div class="modal fade" id="contacta">
        <div class="modal-dialog">
                <div class="modal-content">
                        <div class="modal-header">
                        <a class="close" data-dismiss="modal" >x</a>
                            <!--    <button type="button" class="close" data-dismiss="modal" aria-hidden="true">&times;</button>-->
                               <h4 class="modal-title">Contactar</h4>
                        </div>
                        
              <div class="modal-body">
				<p> Si quieres contactar, completa este formulario y en breve te contestaremos</p>
                <p> Asegurate de que escribes bien tu direccion de correo </p> 
                 <form name="enviaMessage" class="well" id="formularioContacto" >
	       <!--<legend>Contact me</legend>-->
		 <div class="control-group">
                    <div class="controls">
			<input type="text" class="form-control" placeholder="Su nombre" id="nombre2" required data-validation-required-message="Por favor, ponga su nombre" />
			  <p class="help-block"></p>
		   </div>
	         </div> 	
                <div class="control-group">
                  <div class="controls">
			<input type="email" class="form-control" placeholder="Su correo electronico" id="email2" required  data-validation-required-message="Por favor, ponga su correo electronico" />
		</div>
	    </div> 	
        
          <div class="control-group">
                <span class="help-block">Soy de la provincia:</span>
                          
                    
	  <div class="form-group">
                    <select class="form-control"  name="provincia2" id="provincia2" >
                     <!-- <option value=""> Escoje una provincia</option>-->
         				 <?php echo "$lista";?>
                    </select>
                 </div>
                
                
                
               </div> 		 
        
        
		<div class="control-group">
                 <div class="controls">
				 <textarea rows="10" cols="100" class="form-control" 
                       placeholder="Cuentanos el problema" id="texto" required
		       data-validation-required-message="Cuentanos el problema" minlength="5" 
                       data-validation-minlength-message="Min 5 characteres" 
                        maxlength="999" style="resize:none"></textarea>
		</div>
               </div> 
               	 
	     <div id="success2"> </div> <!-- mensajes -->
	    <button type="submit" class="btn btn-primary pull-right">Enviar</button><br />
          </form>
                
                
			</div>
            
			
             
             </div>
        </div>
</div>
<!--
===========================  fin segunda ventana modal
-->

  <div id="footer" class="row">
  <?php  include("temas/$tema_web/pie_com.php"); ?>
   </div>
   
   
   
   
 </div>  
   
    <!--<script src="http://ajax.googleapis.com/ajax/libs/jquery/1.7.1/jquery.min.js"></script>-->
     <script src="js/jquery-1.9.0.min.js"></script>
     <!--<script src="js/jquery.validate.js"></script>-->
	<script src="modulos/bootstrap-3.1.1/js/bootstrap.min.js"></script>
 	<script src="js/jqBootstrapValidation.js"></script>
 	<script src="js/recupera_contrasena.js"></script>
    <script src="js/contact_me.js"></script>
  </body>
  <?php }?>
</html>